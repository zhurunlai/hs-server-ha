﻿using System;
using SQLite;

namespace HsServerHa.Entity.Models
{
    [Table("VirtuallyManage")]
    public class VirtuallyManageEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("CurrIP")]
        public string CurrIP { get; set; }

        [Column("SubnetMask")]
        public string SubnetMask { get; set; }

        [Column("Gateway")]
        public string Gateway { get; set; }

        [Column("DNS1")]
        public string DNS1 { get; set; }

        [Column("DNS2")]
        public string DNS2 { get; set; }

        [Column("MAC")]
        public string MAC { get; set; }

        [Column("VirtuallyIP")]
        public string VirtuallyIP { get; set; }

        [Column("ReferIP")]
        public string ReferIP { get; set; }

        [Column("NetworkCardName")]
        public string NetworkCardName { get; set; }

        [Column("CreateTime")]
        public string CreateTime { get; set; }

        [Column("VirtuallyIPKey")]
        public string VirtuallyIPKey { get; set; }
    }
}
