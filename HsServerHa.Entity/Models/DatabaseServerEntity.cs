﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace HsServerHa.Entity.Models
{
    /// <summary>
    /// 数据库服务器
    /// </summary>
    [Table("DatabaseServer")]
    public class DatabaseServerEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("ProgramKey")]
        public string ProgramKey { get; set; }

        /// <summary>
        /// 服务器类型(主机1 从机2)
        /// </summary>
        [Column("ServerType")]
        public string ServerType { get; set; }

        /// <summary>
        /// 服务器IP
        /// </summary>
        [Column("ServerIp")]
        public string ServerIp { get; set; }

        /// <summary>
        /// 数据库链接字段串
        /// </summary>
        [Column("DbConnectStr")]
        public string DbConnectStr { get; set; }

        /// <summary>
        /// 主机名称
        /// </summary>
        [Column("HostName")]
        public string HostName { get; set; }      

        /// <summary>
        /// 更新时间
        /// </summary>
        [Column("UpdateTime")]
        public string UpdateTime { get; set; }
    }
}
