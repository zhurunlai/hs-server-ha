﻿using SQLite;
using System; 

namespace HsServerHa.Entity.Models
{
    [Table("HeartManage")]
    public class HeartEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("NodeName")]
        public string NodeName { get; set; }

        [Column("HeartName")]
        public string HeartName { get; set; }

        [Column("HeartIP")]
        public string HeartIP { get; set; }

        [Column("CreateTime")]
        public string CreateTime { get; set; }

        [Column("MasterHeartIPBind")]
        public string MasterHeartIPBind { get; set; }

        [Column("IsLocal")]
        public string IsLocal { get; set; }
    }
}
