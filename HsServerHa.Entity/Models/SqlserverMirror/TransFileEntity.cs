﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models.SqlserverMirror
{
    public class TransFileEntity
    {
        public string ServerIp { get; set; }
        public string FilePath { get; set; }

        public int Port { get; set; }
    }
}
