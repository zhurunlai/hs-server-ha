﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models.SqlserverMirror
{
    public class ServiceInfoEntity
    {
        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        public string DBConnectStr { get; set; }

        /// <summary>
        /// 服务器名称
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// 服务器
        /// </summary>
        public string HostName { get; set; }
    }
}
