﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models.SqlserverMirror
{
    public abstract class OperateCommandBase
    {
        public OperateCommandBase() { }

        /// <summary>
        /// 主服务器
        /// </summary>
        public string MainServer { get; set; }

        /// <summary>
        /// 从服务器
        /// </summary>
        public string SlaveServer { get; set; }


        /// <summary>
        /// 主服务器连接
        /// </summary>
        public string MainSqlConnectString { get; set; }

        /// <summary>
        /// 从服务器连接
        /// </summary>
        public string SlaveSqlConnectString { get; set; }

        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// 操作名称
        /// </summary>
        public string Operate { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 端口
        /// </summary>
        public string MainPort {  get; set; }


        /// <summary>
        /// 端口
        /// </summary>
        public string SlavePort { get; set; }

        /// <summary>
        /// 当前操作的命令
        /// </summary>
        public OperateCommand OperateCommand { get; set; }

        public bool OperateResult { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        /// <returns></returns>
        public abstract Task<OperateCommand> operate(ObservableCollection<string> LogTextBox,string path,bool isAutoCopy);

    }
}
