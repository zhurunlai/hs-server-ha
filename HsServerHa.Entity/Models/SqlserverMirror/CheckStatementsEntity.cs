﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models.SqlserverMirror
{
    [Table("ConfigurationStatement")]
    public class CheckStatementsEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }
        [Column("CheckItem")]
        public string CheckItem { get; set; }
        [Column("Statement")]
        public string Statement { get; set; }
        [Column("type")]
        public string type { get; set; }
        [Column("IsCheck")]
        public string IsCheck { get; set; }


        public string Result {  get; set; }


    }


    
   
}
