﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models.SqlserverMirror
{
    /// <summary>
    /// 数据库
    /// </summary>
    public class DataBaseEntity : INotifyPropertyChanged
    {
        /// <summary>
        /// 数据库名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// 数据库状态
        /// </summary>
        public string StateDesc {  get; set; }

        /// <summary>
        /// 镜像类型
        /// </summary>
        public string MirroringRoleDesc { get; set; }

        /// <summary>
        /// 镜像状态
        /// </summary>
        public string MirroringStateDesc { get; set; }

        private string operate { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        public string Operate
        {
            get
            {
                return operate;
            }
            set
            {
                if (operate != value)
                {
                    operate = value;
                    RaisePropertyChanged(nameof(Operate));
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 提供区域性信息
        /// </summary>
        internal CultureInfo CultureInfos = new CultureInfo(CultureInfo.CurrentUICulture.Name);

        /// <summary>
        /// 提供属性更改事件的方法
        /// </summary>
        /// <param name="propertyName"></param>
        internal void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
