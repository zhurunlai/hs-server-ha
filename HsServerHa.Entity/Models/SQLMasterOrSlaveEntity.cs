﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models
{
    public class SQLMasterOrSlaveEntity
    {
        public string DatabaseName { get; set; }
        public string MirroringRole { get; set; }
        public string MirroringRoleDesc { get; set; }
        public string MirroringPartnerName { get; set; }
        public string MirroringWitnessName { get; set; }
        public string MirroringState { get; set; }
        public string MirroringStateDesc { get; set; }
        public string MirroringSafetyLevel { get; set; }
        public string MirroringSafetyLevelDesc { get; set; }
        public string MirroringWitnessState { get; set; }
        public string MirroringWitnessStateDesc { get; set; } 
    }
}
