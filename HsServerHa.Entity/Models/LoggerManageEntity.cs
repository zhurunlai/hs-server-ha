﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace HsServerHa.Entity.Models
{
    [Table("LoggerManage")]
    public class LoggerManageEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("Content")]
        public string Content { get; set; }

        [Column("Type")]
        public string Type { get; set; }

        [Column("CreateTime")]
        public string CreateTime { get; set; }
    }
}
