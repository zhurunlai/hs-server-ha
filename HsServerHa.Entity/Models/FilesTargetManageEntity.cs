﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models
{
    [Table("FilesTargetManage")]
    public class FilesTargetManageEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }
        [Column("Name")]
        public string Name { get; set; }
        [Column("SourceFilePath")]
        public string SourceFilePath { get; set; }

        [Column("TargetFilePath")]
        public string TargetFilePath { get; set; }

        /// <summary>
        /// 文件操作类型（修改-》Changed，新增->Created，删除->Deleted，重命名->Renamed）
        /// </summary>
        [Column("TypeName")]
        public string TypeName { get; set; }

        [Column("ProgramKey")]
        public string ProgramKey { get; set; }

        [Column("CreateTime")]
        public DateTime CreateTime { get; set; }
    }
}
