﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace HsServerHa.Entity.Models
{
    [Table("SQLManage")]
    public class SQLManageEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("ProgramKey")]
        public string ProgramKey { get; set; }

        [Column("DBName")]
        public string DBName { get; set; }

        [Column("Port")]
        public string Port { get; set; }

        [Column("UserId")]
        public string UserId { get; set; }

        [Column("UserPwd")]
        public string UserPwd { get; set; }

        [Column("IsEnable")]
        public string IsEnable { get; set; }

        [Column("CreateTime")]
        public string CreateTime { get; set; }
    }
}
