﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace HsServerHa.Entity.Models
{
    [Table("ProgramManage")]
    public class ProgramManageEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("ProgramKey")]
        public string ProgramKey { get; set; }

        [Column("ProgramName")]
        public string ProgramName { get; set; }

        [Column("Src")]
        public string Src { get; set; }

        [Column("StartupSeq")]
        public string StartupSeq { get; set; }

        [Column("Delayed")]
        public string Delayed { get; set; }

        [Column("IsEnable")]
        public string IsEnable { get; set; }

        [Column("CreateTime")]
        public string CreateTime { get; set; }
    }
}
