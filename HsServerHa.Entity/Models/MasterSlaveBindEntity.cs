﻿using System;
using SQLite;

namespace HsServerHa.Entity.Models
{
    [Table("MasterSlaveBind")]
    public class MasterSlaveBindEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("MasterIP")]
        public string MasterIP { get; set; }

        [Column("SlaveIP")]
        public string SlaveIP { get; set; }

        [Column("IsMaster")]
        public string IsMaster { get; set; }

        [Column("VirtuallyIP")]
        public string VirtuallyIP { get; set; }

        [Column("CreateTime")]
        public string CreateTime { get; set; }
    }
}
