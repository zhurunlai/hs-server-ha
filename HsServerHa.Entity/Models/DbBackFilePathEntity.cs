﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace HsServerHa.Entity.Models
{
    /// <summary>
    /// 数据库服务器
    /// </summary>
    [Table("DbBackFilePath")]
    public class DbBackFilePathEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        /// <summary>
        /// 备份文件路径
        /// </summary>
        [Column("FilePath")]
        public string FilePath { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Column("UpdateTime")]
        public string UpdateTime { get; set; }
    }
}
