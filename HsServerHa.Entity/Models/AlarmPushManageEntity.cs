﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models
{
    [Table("AlarmPushManage")]
    public class AlarmPushManageEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("Api")]
        public string Api { get; set; }

        [Column("Method")]
        public string Method { get; set; }

        [Column("ParamStr")]
        public string ParamStr { get; set; }

        [Column("ProgramKey")]
        public string ProgramKey { get; set; }

        [Column("IsEnable")]
        public string IsEnable { get; set; }

        [Column("CreateTime")]
        public string CreateTime { get; set; }

    }
}
