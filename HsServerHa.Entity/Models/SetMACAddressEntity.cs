﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models
{
    public class SetMACAddressEntity
    {
        public string CardName { get; set; }
        public string MAC { get; set; }
    }
}
