﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models
{
    [Table("FilesSyncManage")]
    public class FilesSyncManageEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("FilePath")]
        public string FilePath { get; set; }

        [Column("Delayed")]
        public string Delayed { get; set; }

        [Column("IsEnable")]
        public string IsEnable { get; set; }

        [Column("CreateTime")]
        public string CreateTime { get; set; }

        [Column("ProgramKey")]
        public string ProgramKey { get; set; }
    }
}
