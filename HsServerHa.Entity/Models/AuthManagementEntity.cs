﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity.Models
{
    public class AuthManagementEntity
    {
        public long Id { get; set; }
        public string Licensekey { get; set; }
        public string RequestCode { get; set; }
        public string ActivationCode { get; set; }
        public string IsUse { get; set; }
        public string CreateTime { get; set; }
    }
}
