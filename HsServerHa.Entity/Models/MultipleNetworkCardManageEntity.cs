﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace HsServerHa.Entity.Models
{
    [Table("MultipleNetworkCardManage")]
    public class MultipleNetworkCardManageEntity
    {
        [PrimaryKey, AutoIncrement]
        [Column("Id")]
        public long Id { get; set; }

        [Column("CurrIP")]
        public string CurrIP { get; set; }

        [Column("RemoteIP")]
        public string RemoteIP { get; set; }

        [Column("SubnetMask")]
        public string SubnetMask { get; set; }

        [Column("Gateway")]
        public string Gateway { get; set; }

        [Column("DNS1")]
        public string DNS1 { get; set; }

        [Column("DNS2")]
        public string DNS2 { get; set; }

        [Column("MAC")]
        public string MAC { get; set; }

        [Column("VirtuallyIP")]
        public string VirtuallyIP { get; set; }

        [Column("ReferIP")]
        public string ReferIP { get; set; }

        [Column("NetworkCardName")]
        public string NetworkCardName { get; set; }

        [Column("IsMaster")]
        public string IsMaster { get; set; }

        [Column("IsEnable")]
        public string IsEnable { get; set; }

        [Column("CreateTime")]
        public string CreateTime { get; set; }

        [Column("ProgramKey")]
        public string ProgramKey { get; set; }
        /// <summary>
        /// 是否同步MAC地址
        /// </summary>

        [Column("IsSyncMAC")]
        public bool IsSyncMAC { get; set; }

        /// <summary>
        /// 当前Mac
        /// </summary>
        [Column("CurrentMAC")]
        public string CurrentMAC { get; set; }
        /// <summary>
        /// 远程Mac
        /// </summary>
        [Column("RemoteMAC")]
        public string RemoteMAC { get; set; }
    }
}
