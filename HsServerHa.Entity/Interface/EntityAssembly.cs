﻿using System.Reflection;

namespace HsServerHa.Entity.Interface
{
    public class EntityAssembly
    {
        /// <summary>
        /// 加载程序集
        /// </summary>
        /// <returns></returns>
        public static Assembly GetAssembly()
        {
            return Assembly.GetExecutingAssembly();
        }
    }
}
