﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Entity
{
    /// <summary>
    /// 页面
    /// </summary>
    public enum PageState
    {
        /// <summary>
        /// 开始页面
        /// </summary>
        BeginPage = 0,
        /// <summary>
        /// 中间页面
        /// </summary>
        MidPage,
        /// <summary>
        /// 尾页面
        /// </summary>
        LastPage  
    }

    public enum OperateType
    {
        Add = 0,
        Delete,
    }

    public enum OperateCommand
    {
        Begin = 0,
        WaitCopyBackUp,
        WaitSetSlaverPartner,
        WaitSetMainPartner,
        Compelete,
        DeleteMainPartner,
        DeleteSlaverPartner

    }
}
