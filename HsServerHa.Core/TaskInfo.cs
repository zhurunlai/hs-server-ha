﻿using System;

namespace HsServerHa.Core
{
    public class TaskInfo
    {
        public int id { get; set; }
        public string TaskName { get; set; }
        public string CronStr { get; set; }
        public string GroupName { get; set; }
    }
}
