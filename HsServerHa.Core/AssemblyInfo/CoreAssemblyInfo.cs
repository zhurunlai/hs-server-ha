﻿using System.Reflection;

namespace HsServerHa.Core.AssemblyInfo
{
    public class CoreAssemblyInfo
    {
        /// <summary>
        /// 加载程序集
        /// </summary>
        /// <returns></returns>
        public static Assembly GetAssembly()
        {
            return Assembly.GetExecutingAssembly();
        }
    }
}
