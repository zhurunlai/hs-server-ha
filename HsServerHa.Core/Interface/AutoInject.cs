﻿namespace HsServerHa.Core.Interface
{
    /// <summary>
    /// 作用域
    /// </summary>
    public interface IAutoInjectScoped { }

    /// <summary>
    /// 瞬时
    /// </summary>
    public interface IAutoInjectTransient { }

    /// <summary>
    /// 单例
    /// </summary>
    public interface IAutoInjectSingleton { }
}
