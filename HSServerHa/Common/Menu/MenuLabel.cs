﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace HsServerHa.Common.Menu
{
    public class MenuLabel
    {
        public string Name { get; set; }
        public List<MenuConfig> MenuData { get; set; }
    }
    public class MenuConfig
    {
        public string Title { get; set; }
        public string Route { get; set; }
        public bool IsSub { get; set; }
        public string Icon { get; set; }
        public bool IsCheck { get; set; }
        public SolidColorBrush CheckColor { get; set; } = Brushes.Black;
        public bool IsExpanded { get; set; }
        public ObservableCollection<MenuConfig> ChildNode { get; set; }
    }
}
