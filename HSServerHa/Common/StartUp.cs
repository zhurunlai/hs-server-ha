﻿using HsServerHa.Core.AssemblyInfo;
using HsServerHa.Entity.Interface;
using HsServerHa.Utility;
using Microsoft.Extensions.DependencyInjection;

namespace HsServerHa.Common
{
    public class StartUp
    {
        /// <summary>
        /// 程序启动
        /// </summary>
        public static void OnStartup()
        {
            var service = new ServiceCollection();
            //注册
            OnRegister(service);
            //创建自动注入
            var autoInjectService = new AutoInject(service);
            //注册程序集
            RegisterBaseAssembly.RegisterService(service);
            //加载BLL程序集
            var assembly = CoreAssemblyInfo.GetAssembly();
            //注册程序集
            autoInjectService.RegisterAssembly(assembly);
            //注册程序集
            assembly = EntityAssembly.GetAssembly();
            autoInjectService.RegisterAssembly(assembly);
            assembly = UtilityAssembly.GetAssembly();
            autoInjectService.RegisterAssembly(assembly);
        }

        /// <summary>
        /// 注册程序集
        /// </summary>
        /// <param name="services"></param>
        private static void OnRegister(IServiceCollection services)
        {
            //注册OPC服务
            //services.AddTransient<IOpcOperator, OpcUaHelp>();
            //services.AddScoped<MqttClientService>();
        }
    }
}
