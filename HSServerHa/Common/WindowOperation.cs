﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace HsServerHa
{
    public class WindowOperation
    {
        /// <summary>
        /// 关闭模板的窗体操作
        /// 朱润来
        /// 2022年10月11日16:48:33
        /// </summary>
        /// <param name="obj"></param>
        public static void CloseWindow(object obj)
        {
            var con = ((obj as UserControl));
            var grid = VisualTreeHelper.GetParent(con);
            var win = VisualTreeHelper.GetParent(grid);
            var a = VisualTreeHelper.GetParent(win);
            ContentPresenter b = VisualTreeHelper.GetParent(a) as ContentPresenter;
            var c = b.TemplatedParent as Window;
            c.Close();
        }
    }
}
