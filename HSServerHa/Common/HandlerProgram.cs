﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Common
{
    public class HandlerProgram
    {
        private static Process Run(string exeName)
        {
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = exeName;
            psi.UseShellExecute = false;
            var src = ($@"{exeName}").Split("\\");
            var exe = src[src.Length - 1];
            psi.WorkingDirectory = exeName.Replace(exe,"");
            psi.CreateNoWindow = true;
            return Process.Start(psi);
        }

        /// <summary>
        /// 开启程序
        /// </summary>
        /// <param name="path">程序路径</param>
        /// <returns></returns>
        public static bool AddProgram(string path)
        {
            Process process = Run(path);
            if (process != null && !process.HasExited)
            {
                App.ProcessList.Add(process);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 检查程序是否运行
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool GetProgramStatus(string FileName) {
            Process process = new Process();
            process.StartInfo.FileName = FileName; //指定要打开的程序文件名
            bool isRunning = !process.HasExited;
            process.Close();
            process.Dispose();
            return isRunning;
        }

        /// <summary>
        /// 检查程序是否运行
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool IsProcessRunning(string FileName)
        { 
            Process[] processes = Process.GetProcesses();
            foreach (Process process in processes)
            {
                if (process.ProcessName == FileName)
                { 
                    return true;
                }
            } 
            return false;
        }

        /// <summary>
        /// 打开程序
        /// </summary>
        /// <param name="FilePath"></param>
        public static void StartProgram(string FilePath)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = FilePath; // 替换为您的WPF程序的路径
            Process.Start(startInfo);
        }

        /// <summary>
        /// 开启隐藏的控制台程序
        /// </summary>
        /// <param name="appPath"></param>
        private static void StartHiddenConsoleApp(string appPath)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = appPath;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;

            Process process = new Process();
            process.StartInfo = startInfo;
            process.Start();
        }

        /// <summary>
        /// 根据进程名称杀死进程
        /// </summary>
        /// <param name="processName"></param>
        public static void KillProcessByName(string processName)
        {
            Process[] processes = Process.GetProcessesByName(processName);

            foreach (Process process in processes)
            {
                process.Kill();
                Console.WriteLine($"Process {process.ProcessName} with ID {process.Id} has been terminated.");
            }
        }

        /// <summary>
        /// 删除所有程序
        /// </summary> 
        /// <returns></returns>
        public static void RemoveProgramAll()
        {
            if (App.ProcessList.Count > 0)
            {
                Process pros = null;
                List<Process> prosNull = new List<Process>();
                foreach (Process pro in App.ProcessList)
                {
                    if (pro.HasExited)
                    {
                        prosNull.Add(pro);
                        continue;
                    }
                    if (pro != null && !pro.HasExited)
                    {
                        pros = pro;
                    }
                }
                foreach (var pro in prosNull)
                {
                    App.ProcessList.Remove(pro);
                }
                if (pros != null)
                {
                    pros.Kill();
                    App.ProcessList.Remove(pros);
                }
            }
        }

        /// <summary>
        /// 根据程序名称删除程序
        /// </summary>
        /// <param name="path">程序</param>
        /// <returns></returns>
        public static bool RemoveProgram(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return false;
            }
            if (App.ProcessList.Count > 0)
            {
                Process pros = null;
                List<Process> prosNull = new List<Process>();
                foreach (Process pro in App.ProcessList)
                {
                    if (pro.HasExited)
                    {
                        prosNull.Add(pro);
                        continue;
                    }

                    if (path.Contains(pro.ProcessName))
                    {
                        if (pro != null && !pro.HasExited)
                        {
                            pros = pro;
                        }
                    }
                }
                foreach (var pro in prosNull)
                {
                    App.ProcessList.Remove(pro);
                }
                if (pros != null)
                {
                    pros.Kill();
                    App.ProcessList.Remove(pros);
                    return true;
                }
            }
            return true;
        }
    }
}
