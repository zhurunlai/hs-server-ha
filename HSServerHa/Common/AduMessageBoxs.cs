﻿using AduSkin.Controls.Metro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HsServerHa
{
    /// <summary>
    /// 
    /// </summary>
    public class AduMessageBoxs
    {
        /// <summary>
        /// 同步执行，需要等待委托执行完成
        /// </summary>
        /// <param name="msg"></param>
        public static void Show(string msg, string caption = "", MessageBoxButton button = MessageBoxButton.OK, MessageBoxImage messageBoxImage = MessageBoxImage.None)
        {

            App.Current.Dispatcher.Invoke((Action)(() =>
            {
                AduMessageBox.Show(msg, caption, button, messageBoxImage);
            }));
        }
    }
}
