﻿using AduSkin.Controls.Metro;
using HsServerHa.Utility.Computer;
using HsServerHa.Utility.DataHelper;
using HsServerHa.WinPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Common.Auth
{
    public class AuthorizationVerification
    {
        /// <summary>
        /// 验证当前是否授权
        /// </summary>
        /// <returns></returns>
        public static bool IsVerification()
        {
            //查找注册表是否存在激活码
            if (string.IsNullOrEmpty(RegistryHelper.ReadRegistryValue("ActivationCode")))
            {
                if (string.IsNullOrEmpty(RegistryHelper.ReadRegistryValue("License")))
                {
                    RegistryHelper.WriteRegistryValue("License", RegistryHelper.EncryptString(RegistryHelper.DecryptString(IniFileConfig.Current.GetStringValue("UserConfig", "PCKey"), RegistryHelper.key), RegistryHelper.key));
                }

                var result = RegistryHelper.VerifyLicense();
                if (!string.IsNullOrEmpty(result))
                {
                    return false;
                }

            }
            else
            {
                if (ComputerHelper.ComputerInfo() != RegistryHelper.DecryptString(RegistryHelper.ReadRegistryValue("ActivationCode"), RegistryHelper.RegisterKey))
                {

                    return false;

                }
            }
            return true;
        }
        /// <summary>
        /// 显示授权过去提示和授权界面
        /// </summary>
        /// <returns></returns>
        public static void ShowAuthBox()
        {
            AduMessageBoxs.Show("授权到期，即将进入授权页面！", "授权异常", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
            AuthManagement auth = new AuthManagement();
            auth.ShowDialog();
        }
    }
}
