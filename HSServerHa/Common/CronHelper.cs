﻿using System;

namespace HsServerHa.Common
{
    public class CronHelper
    {
        public static string DateTime2Cron(DateTime date)
        {
            return date.ToString("ss mm HH dd MM ? yyyy");
        }

        public static DateTime Cron2DateTime(string cron)
        {
            return DateTime.ParseExact(cron, "ss mm HH dd MM ? yyyy", System.Globalization.CultureInfo.CurrentCulture);
        }

        public static DateTimeOffset DateTime2DateTimeOffset(DateTime datetime)
        {
            return DateTime.SpecifyKind(datetime, DateTimeKind.Unspecified);
        }

        public static DateTime DateTimeOffset2DateTime(DateTimeOffset datetimeoffset)
        {
            return datetimeoffset.DateTime;
        }
    }
}
