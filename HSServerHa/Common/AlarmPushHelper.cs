﻿using HsServerHa.Entity.Models;
using HsServerHa.Utility.HttpHelper;
using HsServerHa.Utility.SQLite;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Common
{
    public class AlarmPushHelper
    {
        public static async Task PushAlarm(string msg) {
            var list = SQLiteHelper.Table<AlarmPushManageEntity>().Where(x => x.IsEnable == "是").ToList();
            foreach (var item in list)
            {
                var paramStr=item.ParamStr.Replace("#Params#", msg).ToString();
                var result =await HttpService.Instance.Post(item.Api, paramStr).ConfigureAwait(false);
                if (result==null)
                {
                    LoggerHelper.Error($"Api调用失败，参数:（{JsonConvert.SerializeObject(item)}）,告警信息:"+msg, "告警推送");
                }
                else
                {
                    LoggerHelper.Info($"Api调用成功，参数:（{JsonConvert.SerializeObject(item)}）,告警信息:" + msg, "告警推送");
                }
            }
        }
    }
}
