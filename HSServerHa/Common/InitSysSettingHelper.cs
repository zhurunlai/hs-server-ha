﻿using HsServerHa.Entity.Models;
using HsServerHa.Utility.DataHelper;
using HsServerHa.Utility.NetWork;
using HsServerHa.Utility.SQLite;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Common
{
    public class InitSysSettingHelper
    {
        public static void InitSys() {
            //var sqlVM = "select * from VirtuallyManage";
            //var dt = DbHelperSQLite.Query(sqlVM)?.Tables?[0];
            //List<VirtuallyManageEntity> list = DataHelpers.ConvertToModel<VirtuallyManageEntity>(dt);
            List<VirtuallyManageEntity> list= SQLiteHelper.Table<VirtuallyManageEntity>().ToList();
            var msFirst = list.FirstOrDefault();
            if (msFirst != null)
            {
                //切换网络
                //Process process = new Process();
                //ProcessStartInfo startInfo = new ProcessStartInfo();
                //startInfo.FileName = "cmd.exe";
                //startInfo.Arguments = $"/c" +
                //    $" netsh interface ip set address name={msFirst.NetworkCardName} source=static address={msFirst.CurrIP} mask={msFirst.SubnetMask} gateway={msFirst.Gateway}";
                //+
                //$"& netsh interface ipv4 delete address name={msFirst.NetworkCardName}  addr={msFirst.VirtuallyIP}  gateway=all " +
                //$"  & netsh interface set interface {msFirst.NetworkCardName} admin=disable && netsh interface set interface {msFirst.NetworkCardName} admin=enable";

                //startInfo.UseShellExecute = false;
                //startInfo.RedirectStandardInput = false;
                //startInfo.RedirectStandardOutput = true;
                //startInfo.CreateNoWindow = true;
                //process.StartInfo = startInfo;

                //startInfo.Verb = "RunAs";
                //process.Start();

                NetWorkHelper.DeleteVirtualIP(msFirst?.VirtuallyIP);
            }

            DbHelperSQLite.ExecuteSql("delete from HeartManage");
            DbHelperSQLite.ExecuteSql("delete from MasterSlaveBind");
            DbHelperSQLite.ExecuteSql("delete from VirtuallyManage");
            //DbHelperSQLite.ExecuteSql("delete from SQLManage");
            DbHelperSQLite.ExecuteSql("delete from ProgramManage");
            DbHelperSQLite.ExecuteSql("delete from MultipleNetworkCardManage");
            IniFileConfig.Current.SetStringValue("UserConfig", "IsMainPC", "");
            IniFileConfig.Current.SetStringValue("UserConfig", "UserName", "admin");
            IniFileConfig.Current.SetStringValue("UserConfig", "UserPwd", "1");
            IniFileConfig.Current.SetStringValue("UserConfig", "IsStartSyncFile", "true");
            IniFileConfig.Current.SetStringValue("UserConfig", "isStartDaemon", "true"); 


            try
            {
                LoggerHelper.SetInfo("初始化系统设置->当前系统为：" + App.isMaster == null ? "无状态" : (App.isMaster.Value ? "主机" : "从机"), "初始化系统设置");
            }
            catch (Exception ex)
            { 

            }
          
            App.isMaster = null;
            App.socketServer = null;
            App.proxSocket = null;
            App.XtIP = null;
            App.GatewayIP = null;



            System.Windows.Forms.Application.Restart();

            Environment.Exit(0);
        }

        /// <summary>
        /// 程序自启动
        /// </summary>
        /// <param name="enable"></param>
        public static void SetAutoStart(bool enable)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (enable)
            { 
                var FileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                LoggerHelper.Info(FileName, "SetAutoStart");
                key.SetValue("HsServerHa", FileName+ " /autorun");
            }
            else
            {
                key.DeleteValue("HsServerHa", false);
            }
        }

        /// <summary>
        /// 获取程序是否开机自启
        /// </summary>
        public static bool GetAutoStart() {
            var result = false;
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (key.GetValue("HsServerHa") == null)
            {
                result = false;
            }
            else
            {
                result = true;
            }
            return result;
        }
    }
}
