﻿using AduSkin.Controls.Metro;
using HsServerHa.Common.Auth;
using HsServerHa.Utility.Computer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HsServerHa.WinPage
{
    /// <summary>
    /// About.xaml 的交互逻辑
    /// </summary>
    public partial class About : MetroWindow
    {
        public About()
        {
            InitializeComponent();
            this.Copy.Content = $"Copyright © 2023-{DateTime.Now.Year} 华数锦明 保留所有权利";
            if (AuthorizationVerification.IsVerification()) {
                if (string.IsNullOrEmpty(RegistryHelper.ReadRegistryValue("ActivationCode"))) {
                    this.AuthLab.Content = "未授权";
                    this.AuthLab.Foreground = Brushes.Red;
                }
                else
                {
                    this.AuthLab.Content = "已授权";
                    this.AuthLab.Foreground = Brushes.Green;
                }
            }
            else
            {
                this.AuthLab.Content = "未授权";
                this.AuthLab.Foreground = Brushes.Red;
            }

         
        }
    }
}
