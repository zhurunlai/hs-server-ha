﻿
using AduSkin.Controls.Metro;
using HsServerHa.ViewModel;
using System.Windows.Controls;

namespace HsServerHa.WinPage
{
    /// <summary>
    /// AlertTemp.xaml 的交互逻辑
    /// </summary>
    public partial class AlertTemp : MetroWindow
    {
        public AlertTemp(UserControl control, string Title, string Width, string Height)
        {
            InitializeComponent();
            this.DataContext = new AlertTempViewModel(control, Title, Width, Height);
        }
    }
}
