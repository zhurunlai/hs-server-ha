﻿using AduSkin.Controls.Metro;
using HsServerHa.Common.Auth;
using HsServerHa.Utility.Computer;
using HsServerHa.Utility.DataHelper;
using Quartz.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HsServerHa.WinPage
{
    /// <summary>
    /// AuthManagement.xaml 的交互逻辑
    /// </summary>
    public partial class AuthManagement : MetroWindow
    {
        public AuthManagement()
        {
            InitializeComponent();
            if (AuthorizationVerification.IsVerification())
            {
                if (string.IsNullOrEmpty(RegistryHelper.ReadRegistryValue("ActivationCode")))
                {
                    this.OKAuth.Visibility = Visibility.Hidden;
                    this.NoAuth.Visibility = Visibility.Visible;
                    string savedDate = RegistryHelper.ReadRegistryValue("License");
                    savedDate = RegistryHelper.DecryptString(savedDate, RegistryHelper.key);
                    DateTime cd = DateTime.ParseExact(savedDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                    DateTime hd = DateTime.ParseExact(RegistryHelper.DecryptString(IniFileConfig.Current.GetStringValue("UserConfig", "PCKey"), RegistryHelper.key), "yyyyMMdd", CultureInfo.InvariantCulture);
                    var day = ConfigurationManager.AppSettings["TestDay"]?.ToInt() ?? 15;
                    if (cd > hd.AddDays(day))
                    {
                        this.OnTrial.Content = "已到期";
                        return;
                    }
                    else
                    {
                        TimeSpan t = hd.AddDays(day).Subtract(cd);
                        this.OnTrial.Content = t.Days + "天";
                    }

                }
                else
                {
                    string licenseKey = RegistryHelper.ReadRegistryValue("LicenseKey");
                    this.LicenseLab.Content = licenseKey;
                    this.OKAuth.Visibility = Visibility.Visible;
                    this.NoAuth.Visibility = Visibility.Hidden;
                }

            }
            else
            {
                this.OnTrial.Content = "已到期";
                this.OKAuth.Visibility = Visibility.Hidden;
                this.NoAuth.Visibility = Visibility.Visible;
            }
        }

        private void btn_Setting_Click(object sender, RoutedEventArgs e)
        {
            //解析ActivationCode
            var ActivationCode = this.ActivationCode.Text;
            var RequestCode = this.RequestCode.Text;
            if (string.IsNullOrEmpty(this.LicenseKey.Text))
            {
                AduMessageBoxs.Show("LicenseKey不能为空！");
                return;
            }
            if (string.IsNullOrEmpty(RequestCode))
            {
                AduMessageBoxs.Show("请求编码不能为空！");
                return;
            }
            if (string.IsNullOrEmpty(ActivationCode))
            {
                AduMessageBoxs.Show("激活编码不能为空！");
                return;
            }
            string? jmPcInfo;
            try
            {
                jmPcInfo = RegistryHelper.DecryptString(ActivationCode, RegistryHelper.RegisterKey);
            }
            catch (Exception ex)
            {
                AduMessageBoxs.Show("激活失败！ " + ex.Message);
                return;
            }

            if (string.IsNullOrEmpty(jmPcInfo))
            {
                AduMessageBoxs.Show("激活失败，请重试！");
                return;
            }
            var str = ComputerHelper.ComputerInfo();
            if (str.Equals(jmPcInfo))
            {
                AduMessageBoxs.Show("激活成功！");
                RegistryHelper.WriteRegistryValue("ActivationCode", ActivationCode);
                RegistryHelper.WriteRegistryValue("LicenseKey", this.LicenseKey.Text);

                if (AuthorizationVerification.IsVerification())
                {
                    if (string.IsNullOrEmpty(RegistryHelper.ReadRegistryValue("ActivationCode")))
                    {
                        this.OKAuth.Visibility = Visibility.Hidden;
                        this.NoAuth.Visibility = Visibility.Visible;
                        string savedDate = RegistryHelper.ReadRegistryValue("License");
                        savedDate = RegistryHelper.DecryptString(savedDate, RegistryHelper.key);
                        DateTime cd = DateTime.ParseExact(savedDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                        DateTime hd = DateTime.ParseExact(RegistryHelper.DecryptString(IniFileConfig.Current.GetStringValue("UserConfig", "PCKey"), RegistryHelper.key), "yyyyMMdd", CultureInfo.InvariantCulture);
                        var day = ConfigurationManager.AppSettings["TestDay"]?.ToInt() ?? 15;
                        if (cd > hd.AddDays(day))
                        {
                            this.OnTrial.Content = "已到期";
                            return;
                        }
                        else
                        {
                            TimeSpan t = hd.AddDays(day).Subtract(cd);
                            this.OnTrial.Content = t.Days + "天";
                        }

                    }
                    else
                    {
                        string licenseKey = RegistryHelper.ReadRegistryValue("LicenseKey");
                        this.LicenseLab.Content = licenseKey;
                        this.OKAuth.Visibility = Visibility.Visible;
                        this.NoAuth.Visibility = Visibility.Hidden;
                    }

                }
                return;
            }
            else
            {
                AduMessageBoxs.Show("LicenseKey或激活码不正确！");
                return;

            }
        }



        private void MetroTextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (this.LicenseKey.Text.Length != 16)
                {
                    AduMessageBoxs.Show("LicenseKey格式不正确！");
                    return;
                }
                var str = ComputerHelper.ComputerInfo();
                this.RequestCode.Text = RegistryHelper.EncryptString(this.LicenseKey.Text + str, RegistryHelper.key);
            }
        }
    }
}
