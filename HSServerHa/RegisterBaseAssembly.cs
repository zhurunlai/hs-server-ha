﻿using HsServerHa.Core.Interface;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace HsServerHa
{
    public class RegisterBaseAssembly
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public static void RegisterService(IServiceCollection serviceCollection)
        {
            ////添加注入HTTP服务
            //serviceCollection.AddHttpClient();
            ////注入HTTP请求类
            //serviceCollection.AddScoped<RequestToHttpHelper>();

            //自动注入全局继承IAutoInjectScoped、IAutoInjectTransient、IAutoInjectSingleton
            //朱润来 2022年8月10日09:31:29
            var serviceScopedAll = AppDomain.CurrentDomain.GetAssemblies()
              .SelectMany(x => x.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IAutoInjectScoped))));
            var serviceTransientAll = AppDomain.CurrentDomain.GetAssemblies()
              .SelectMany(x => x.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IAutoInjectTransient))));
            var serviceSingletonAll = AppDomain.CurrentDomain.GetAssemblies()
              .SelectMany(x => x.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IAutoInjectSingleton))));

            foreach (var item in serviceScopedAll)
            {
                serviceCollection.AddScoped(item);
            }
            foreach (var item in serviceTransientAll)
            {
                serviceCollection.AddTransient(item);
            }
            foreach (var item in serviceSingletonAll)
            {
                serviceCollection.AddSingleton(item);
            }
        }
    }
}
