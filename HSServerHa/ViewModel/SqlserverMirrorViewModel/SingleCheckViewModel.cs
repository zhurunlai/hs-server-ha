﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging.Messages;
using CommunityToolkit.Mvvm.Messaging;
using HsServerHa.Core.Http;
using HsServerHa.Entity.Models.SqlserverMirror;
using HsServerHa.Utility.HttpHelper;
using HsServerHa.Utility.SQLite;
using HsServerHa.Utility.SqlserverMirror;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using AduSkin.Controls.Metro;
using HsServerHa.Entity.Models;
using System.Windows;

namespace HsServerHa.ViewModel.SqlserverMirrorViewModel
{
    public class SingleCheckViewModel : ObservableObject
    {
        public SingleCheckViewModel() {
            checkStatementsEntities = new ObservableCollection<CheckStatementsEntity>();
           
        }
        ObservableCollection<CheckStatementsEntity> checkStatementsEntities;


        public ObservableCollection<CheckStatementsEntity> CheckStatementsEntities {
            get { return checkStatementsEntities; }
            set
            {
                checkStatementsEntities = value;
                OnPropertyChanged("CheckStatementsEntities");
            }

        }

        public ServiceInfoEntity SerInfo { get; set; } = new ServiceInfoEntity();

        /// <summary>
        /// 检查配置项
        /// </summary>
        /// <returns></returns>
        public bool DBCheck()
        {
            CheckStatementsEntities.Clear();
            CheckStatementsEntities = CheckStatementCommand.DBCheck(SerInfo.DBConnectStr);
            return CheckStatementsEntities.DBCheckResult();
        }

        /// <summary>
        /// 配置结果检查
        /// </summary>
        /// <returns></returns>
        public bool CheckResult()
        {
            if (!CheckStatementsEntities.DBCheckResult())
                return false;
            return true;
        }

        /// <summary>
        /// 重新配置镜像
        /// </summary>
        /// <param name="FolderPath"></param>
        /// <returns></returns>
        public bool ResetMirror(string FolderPath = "d:\\dbback")
        {
            CheckStatementsEntities.Clear();
            CheckStatementsEntities = CheckStatementCommand.ResetMirror(SerInfo, FolderPath);
            if (!CheckStatementsEntities.DBCheckResult())
                return false;
            return true;
        }

        /// <summary>
        /// 配置入站
        /// </summary>
        /// <param name="FolderPath"></param>
        /// <returns></returns>
        public bool ImportCertificate(string FolderPath = "d:\\dbback")
        {
            CheckStatementsEntities = CheckStatementCommand.ImportCertificate(SerInfo, FolderPath);
            if (!CheckStatementsEntities.DBCheckResult())
                return false;
            return true;
        }
    }
}
