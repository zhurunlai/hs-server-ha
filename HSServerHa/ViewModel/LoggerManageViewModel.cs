﻿using AduSkin.Controls.Data;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HsServerHa.Entity.Models;
using HsServerHa.Utility.DataHelper;
using HsServerHa.Utility.SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace HsServerHa.ViewModel
{
    public class LoggerManageViewModel : ObservableObject
    {
        public LoggerManageViewModel() {
            PageNum = 10;
            PageIndex = 1;
            InitData();
             
            QueryCommand = new RelayCommand(InitData); 
        }
        public async void InitData()
        {


            //var dataSet = DbHelperSQLite.Query("SELECT * FROM LoggerManage ", null);
            //DataTable dt = dataSet.Tables[0];
            //修改日志排序为按创建时间倒序
            List<LoggerManageEntity> list = (await SQLiteHelper.TableAsync<LoggerManageEntity>().ToListAsync().ConfigureAwait(false)).OrderByDescending(s => s.Id).ToList();

            Total =  list.Count > 0 ? (int)Math.Ceiling((decimal)list.Count / PageNum) : 0;

            list = list.Skip(PageNum * (PageIndex - 1)).Take(PageNum).ToList();
            LogList = new ObservableCollection<LoggerManageEntity>(list);
        }

        public RelayCommand<FunctionEventArgs<int>> PageUpdatedCmd =>
                new Lazy<RelayCommand<FunctionEventArgs<int>>>(() =>
                new RelayCommand<FunctionEventArgs<int>>(PageUpdated)).Value;
        public void PageUpdated(FunctionEventArgs<int> info)
        {
            PageIndex = info.Info;
            InitData();
        }

        public ObservableCollection<LoggerManageEntity> logList { get; set; }
        public ObservableCollection<LoggerManageEntity> LogList
        {
            get { return logList; }
            set
            {
                logList = value;
                OnPropertyChanged("LogList");
            }
        }

        public int total { get; set; }
        public int Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
                OnPropertyChanged("Total");
            }
        }

        public int pageIndex { get; set; }
        public int PageIndex
        {
            get
            {
                return pageIndex;
            }
            set
            {
                pageIndex = value;
                OnPropertyChanged("PageIndex");
            }
        }

 

        public int pageNum { get; set; }
        public int PageNum
        {
            get
            {
                return pageNum;
            }
            set
            {
                pageNum = value;
                OnPropertyChanged("PageNum");
            }
        }

        public RelayCommand QueryCommand { get; set; }
    }
}
