﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.Mvvm.Messaging.Messages;
using HsServerHa.Entity.Models;
using HsServerHa.UserControls.Dialog;
using HsServerHa.Utility.DataHelper;
using HsServerHa.Utility.SQLite;
using HsServerHa.WinPage;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;

namespace HsServerHa.ViewModel
{
    public class AddNodeViewModel : ObservableObject
    {
        public AddNodeViewModel()
        {
            InitData();

            AddCommand = new RelayCommand(addFun);
            QueryCommand = new RelayCommand(InitData);
            UpdateCommand = new RelayCommand(updateFun);

            WeakReferenceMessenger.Default.Register<ValueChangedMessage<string>, string>(this, "RefreshHeart", (r, message) =>
            {
                if (message != null)
                {
                    if (message.Value == "刷新心跳数据")
                    {
                        InitData();
                    }
                }
            });

        }

        public   void InitData()
        {
           //var lists=await  SQLiteHelper.TableAsync<HeartEntity>().ToListAsync().ConfigureAwait(false);
           // var dataSet = DbHelperSQLite.Query("SELECT * FROM HeartManage ", null);
           // DataTable dt = dataSet.Tables[0];
            List<HeartEntity> list = SQLiteHelper.Table<HeartEntity>().ToList();

            HeartList = new ObservableCollection<HeartEntity>(list);
        }

        public ObservableCollection<HeartEntity> heartList { get; set; }
        public ObservableCollection<HeartEntity> HeartList
        {
            get { return heartList; }
            set
            {
                heartList = value;
                OnPropertyChanged("HeartList");
            }
        }

        public void addFun()
        {
            AlertTemp alertTempJoinMaster = new AlertTemp(new AddHeart(), "添加心跳", "500", "800");
            alertTempJoinMaster.ShowDialog();
        }
        public void updateFun()
        {

            AlertTemp alertTempJoinMaster = new AlertTemp(new AddHeart(), "修改心跳", "500", "800");
            alertTempJoinMaster.ShowDialog();
        }
        public RelayCommand AddCommand { get; set; }
        public RelayCommand QueryCommand { get; set; }
        public RelayCommand UpdateCommand { get; set; }
    }
}
