﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.Windows.Controls;

namespace HsServerHa.ViewModel
{
    public class AlertTempViewModel : ObservableObject
    {
        public AlertTempViewModel(UserControl control, string Title, string Width, string Height)
        {
            this.CurrentPage = control;
            this.Title = Title;
            this.Width = Width;
            this.Height = Height;
        }

        public UserControl currentPage { get; set; }
        public UserControl CurrentPage
        {
            get { return currentPage; }
            set
            {
                currentPage = value;
                OnPropertyChanged("CurrentPage");
            }
        }

        public string title { get; set; }
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged("Title");
            }
        }

        public string width { get; set; } = "800";
        public string Width
        {
            get { return width; }
            set
            {
                width = value;
                OnPropertyChanged("Width");
            }
        } 

        public string height { get; set; } = "500";
        public string Height
        {
            get { return height; }
            set
            {
                height = value;
                OnPropertyChanged("Height");
            }
        } 
    }
}
