﻿using AduSkin.Controls.Metro;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using HsServerHa.UserControls;
using HsServerHa.Utility.DataHelper;
using Newtonsoft.Json;
using System.Windows;

namespace HsServerHa.ViewModel
{
    public class LoginViewModel : ObservableObject
    {
        public LoginViewModel()
        {

            LoginCommand = new RelayCommand<object>(LoginInfo);
        }

        public async void LoginInfo(object obj)
        {

            if (string.IsNullOrWhiteSpace(UserName))
            {
                AduMessageBoxs.Show("账号不能为空！");
                return;
            }
            if (string.IsNullOrWhiteSpace(Password))
            {
                AduMessageBoxs.Show("密码不能为空！");
                return;
            }
            var uName = IniFileConfig.Current.GetStringValue("UserConfig", "UserName");
            var uPwd = IniFileConfig.Current.GetStringValue("UserConfig", "UserPwd");
            if (uName == UserName && uPwd == Password)
            {
                App.isLogin = true;
                var win = ((obj as Window));
                var isMainPC = IniFileConfig.Current.GetStringValue("UserConfig", "IsMainPC");
                if (string.IsNullOrEmpty(isMainPC))//如果没设置就跳到设置界面
                {
                    LoggerHelper.SetInfo("登录成功", "登录");
                    SetMasterSlave setMasterSlave = new SetMasterSlave();
                    setMasterSlave.Show();
                    setMasterSlave.Activate();
                    win.IsEnabled = true;
                    win.Hide();
                }
                else
                {//直接到首页
                    LoggerHelper.SetInfo("登录成功", "登录");

                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                    mainWindow.Activate();
                    win.IsEnabled = true;
                    win.Hide();
                }
            }
            else
            {
                LoggerHelper.SetInfo($"账户或者密码错误->账户：{UserName}，密码：{Password}", "登录");
                AduMessageBoxs.Show("账户或者密码错误！");
            }
        }
        public RelayCommand<object> LoginCommand { get; set; }

        public string userName { get; set; }
        public string UserName
        {
            get
            {
                return userName;


            }
            set
            {
                userName = value;
                OnPropertyChanged("UserName");
            }
        }

        public string password { get; set; }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                OnPropertyChanged("Password");
            }
        }

        public string realName { get; set; }
        public string RealName
        {
            get
            {
                return realName;
            }
            set
            {
                realName = value;
                OnPropertyChanged("RealName");
            }
        }
    }
}
