﻿using AduSkin.Controls.Metro;
using HsServerHa.Utility.NetWork;
using HsServerHa.ViewModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace HsServerHa
{
    /// <summary>
    /// Login.xaml 的交互逻辑
    /// </summary>
    public partial class Login : MetroWindow
    {
        public Login()
        {
            InitializeComponent();
            this.DataContext = new LoginViewModel();
            this.UserName.Focus();

        }

        private async void AduFlatButton_Click(object sender, RoutedEventArgs e)
        {
            this.loading.Visibility = Visibility.Visible;
            this.login.IsEnabled = false;
            await Task.Delay(2000);
            this.loading.Visibility = Visibility.Hidden;
            this.login.IsEnabled = true;

        }


        private void login_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LoginViewModel loginViewModel = new LoginViewModel();
                loginViewModel.UserName = this.UserName.Text;
                loginViewModel.Password = this.Password.Password;
                object[] obj = new object[] { this };
                loginViewModel.LoginInfo(this);

            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
           Application.Current.Shutdown();
        }
    }
}
