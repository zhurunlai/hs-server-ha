﻿using AduSkin.Controls.Metro;
using CommunityToolkit.Mvvm.Messaging;
using HsServerHa.Common;
using HsServerHa.Entity.Models;
using HsServerHa.Utility.DataHelper;
using HsServerHa.Utility.NetWork;
using HsServerHa.Utility.SQLite;
using HsServerHa.ViewModel;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace HsServerHa
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (InitSysSettingHelper.GetAutoStart())
                {
                    LoggerHelper.Info("准备插入开机自启", "SetAutoStart");
                    // 开启自启动
                    InitSysSettingHelper.SetAutoStart(true);
                    LoggerHelper.Info("执行成功", "SetAutoStart");
                  
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SetAutoStart");
            }
         
        
        }

        private NotifyIcon notifyIcon;

        public MainWindow()
        {
            this.Loaded += this.Window_Loaded;
            if (!App.isLogin)
            {
                Login l = new Login();
                l.Show();
                this.Hide();
            }
            InitializeComponent();
            //notifyIcon = new NotifyIcon();
            ////notifyIcon.Icon =ImageSource;  // 设置托盘图标
            //notifyIcon.Visibility = Visibility.Visible;
            //notifyIcon.Text = "My WPF App";  // 设置鼠标悬停时显示的文本

            //notifyIcon.MouseDoubleClick += (s, e) =>
            //{
            //    // 处理双击事件
            //    // 例如：显示主窗口
            //    this.Show();
            //    this.WindowState = WindowState.Normal;
            //};


            this.DataContext = new MainWindowViewModel();

            this.Default.Foreground = Brushes.Red;
            this.Default.Background = Brushes.Red;

            var sqlVM = "select * from VirtuallyManage";
            var dt = DbHelperSQLite.Query(sqlVM)?.Tables?[0];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    List<VirtuallyManageEntity> list = DataHelpers.ConvertToModel<VirtuallyManageEntity>(dt);
                    HttpServerService.Start(list[0].NetworkCardName);


                }
            }
        }



        private void ChangeMenu_Click(object sender, EventArgs e)
        {
            var tag = (sender as MetroExpander).Tag.ToString();
            if (!string.IsNullOrEmpty(tag))
            {
                this.tabName.Content = (sender as MetroExpander).Header.ToString().Trim();
                if (tag == "DefaultPage")
                {

                    this.Default.Foreground = Brushes.Red;
                    this.Default.Background = Brushes.Red;
                }
                else
                {
                    this.Default.Foreground = Brushes.Black;
                }

            }
            WeakReferenceMessenger.Default.Send(tag, "MenuData");
        }



        private void showMain_Click(object sender, RoutedEventArgs e)
        {
            this.Show();
            this.Activate();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

    }
}
