﻿using AduSkin.Controls.Metro;
using HsServerHa.Common.Auth;
using HsServerHa.Utility.DataHelper;
using System.Windows;
using System.Windows.Controls;

namespace HsServerHa.UserControls
{
    /// <summary>
    /// PassworkEdit.xaml 的交互逻辑
    /// </summary>
    public partial class PassworkEdit : UserControl
    {
        public PassworkEdit()
        {
            InitializeComponent();
        }

        private void btn_Setting_Click(object sender, RoutedEventArgs e)
        {
            #region 授权验证
            if (!AuthorizationVerification.IsVerification())
            {
                AduMessageBoxs.Show("授权到期！");
                return;
            }

            #endregion
            if (string.IsNullOrEmpty(OldPwd.Password))
            {
                AduMessageBoxs.Show("请输入旧密码");
                return;
            }
            if (string.IsNullOrEmpty(NewPwd.Password))
            {
                AduMessageBoxs.Show("请输入新密码");
                return;
            }
            if (string.IsNullOrEmpty(VerilyNewPwd.Password))
            {
                AduMessageBoxs.Show("请输入验证的新密码");
                return;
            }
            if (NewPwd.Password != VerilyNewPwd.Password)
            {
                AduMessageBoxs.Show("两次输入的密码不一致");
                return;
            }
            MessageBoxResult dr = AduMessageBox.ShowOKCancel($"你确定修改密码？", "系统提示!", "确定", "取消");
            if (dr == MessageBoxResult.OK)
            {
                var uName = IniFileConfig.Current.GetStringValue("UserConfig", "UserName");
                var uPwd = IniFileConfig.Current.GetStringValue("UserConfig", "UserPwd");
                if (OldPwd.Password != uPwd)
                {
                    AduMessageBoxs.Show("输入的旧密码不正确");
                    return;
                }
                IniFileConfig.Current.SetStringValue("UserConfig", "UserPwd", NewPwd.Password);
                AduMessageBoxs.Show("密码修改成功！");
                LoggerHelper.SetInfo("密码修改成功！", "密码修改");
            }


        }
    }
}
