﻿using HsServerHa.ViewModel;
using System;
using System.Windows.Controls;

namespace HsServerHa.UserControls
{
    /// <summary>
    /// AddNode.xaml 的交互逻辑
    /// </summary>
    public partial class AddNode : UserControl
    {
        public AddNode()
        {
            InitializeComponent();
            this.DataContext = new AddNodeViewModel();

            this.MyDataGrid.LoadingRow += new EventHandler<DataGridRowEventArgs>(this.dataGridEquipment_LoadingRow);
        }

        private void dataGridEquipment_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex() + 1;
        }
    }
}
