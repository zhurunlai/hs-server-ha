﻿using AduSkin.Controls.Metro;
using HsServerHa.Common;
using HsServerHa.Common.Auth;
using HsServerHa.Core.Http;
using HsServerHa.Entity.Models;
using HsServerHa.Utility.DataHelper;
using HsServerHa.Utility.HttpHelper;
using HsServerHa.Utility.NetWork;
using HsServerHa.Utility.SQLite;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Windows;

namespace HsServerHa.UserControls
{
    /// <summary>
    /// InitSysSetting.xaml 的交互逻辑
    /// </summary>
    public partial class InitSysSetting : System.Windows.Controls.UserControl
    {
        public InitSysSetting()
        {
            InitializeComponent();
            initData();
        }
        public void initData()
        {
            if (App.isMaster != null)
            {
                this.btn_Setting.Visibility = Visibility.Visible;
                if (App.isMaster.Value)
                {
                    this.zj_grid.Visibility = Visibility.Visible;
                    this.cj_grid.Visibility = Visibility.Hidden;
                }
                else
                {
                    this.zj_grid.Visibility = Visibility.Hidden;
                    this.cj_grid.Visibility = Visibility.Visible;
                }
            }
            else
            {
                this.btn_Setting.Visibility = Visibility.Hidden;
            }
        }
        private async void btn_Setting_Click(object sender, RoutedEventArgs e)
        {
            #region 授权验证
            if (!AuthorizationVerification.IsVerification())
            {
                AduMessageBoxs.Show("授权到期！");
                return;
            }

            #endregion
            MessageBoxResult dr = AduMessageBox.ShowOKCancel($"你确定选择初始化系统吗(请单独初始化数据库镜像)？", "系统提示!", "确定", "取消");

            if (dr == MessageBoxResult.OK)
            {
                if (App.isMaster != null)
                {
                    //var sqlMaster2 = "SELECT * FROM  MasterSlaveBind where 1=1";
                    //var dtMaster2 = DbHelperSQLite.Query(sqlMaster2)?.Tables?[0];
                    //var masterList = DataHelpers.ConvertToModel<MasterSlaveBindEntity>(dtMaster2);

                    var masterList =   SQLiteHelper.Table<MasterSlaveBindEntity>().ToList();
                    var masterFirst = masterList.FirstOrDefault();

                    var masterPath = "";
                    if (masterFirst != null)
                    {
                        if (App.isMaster.Value)
                        {
                            masterPath = $"http://{masterFirst.SlaveIP}:{ConfigurationManager.AppSettings["HttpPort"]?.ToString() ?? "6601"}/";
                        }
                        else
                        {
                            masterPath = $"http://{masterFirst.MasterIP}:{ConfigurationManager.AppSettings["HttpPort"]?.ToString() ?? "6601"}/";
                        }
                        await HttpHelpers.PostToJson<HttpResponseResultModel<bool>>(masterPath + $"{ConfigurationManager.AppSettings["InitSysInfo"]?.ToString() ?? "InitSysInfo"}").ConfigureAwait(false);
                    }
                   
                }
              
                InitSysSettingHelper.InitSys();

            }
        }
    }
}
