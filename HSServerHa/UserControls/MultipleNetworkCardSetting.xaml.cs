﻿using HsServerHa.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HsServerHa.UserControls
{
    /// <summary>
    /// MultipleNetworkCardSetting.xaml 的交互逻辑
    /// </summary>
    public partial class MultipleNetworkCardSetting : UserControl
    {
        public MultipleNetworkCardSetting()
        {
            InitializeComponent();
            this.DataContext = new MultipleNetworkCardSettingViewModel();
        }
    }
}
