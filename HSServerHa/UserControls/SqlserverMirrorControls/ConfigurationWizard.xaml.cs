﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using CommunityToolkit.Mvvm.Messaging;
using HsServerHa.Entity;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace HsServerHa.UserControls.SqlserverMirrorControls
{
    /// <summary>
    /// ConfigurationWizard.xaml 的交互逻辑
    /// </summary>
    public partial class ConfigurationWizard : UserControl
    {
        public ConfigurationWizard()
        {
            InitializeComponent();
            wizardFrame.NavigationService.Navigated += NavigationService_Navigated;
            //wizardFrame.NavigationService.Navigate(new ParameterSettings()); // 初始页面
            wizardFrame.NavigationService.Navigate(CurrentPage); // 初始页面WelcomePage.xaml
            ButtonStateChange(PageState.BeginPage);
            WeakReferenceMessenger.Default.Register<ValueChangedMessage<string>, string>(this, "NextButtonIsEnable", (r, message) =>
            {
                if (message != null)
                {
                    if (message.Value == "启用")
                    {
                        this.ForwardPageBtn.IsEnabled = true;
                    }
                    else
                    {
                        this.ForwardPageBtn.IsEnabled = false;
                    }
                }
            });
            WeakReferenceMessenger.Default.Register<ValueChangedMessage<bool>, string>(this, "BackwardPageBtnIsEnable", (r, message) =>
            {
                if (message != null)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        this.BackwardPageBtn.IsEnabled = message.Value;
                    });
                }
            });
        }

        Page CurrentPage = new WelcomePage();

        private void ButtonStateChange(PageState PageState)
        {
            switch (PageState)
            {
                case PageState.BeginPage:

                    this.BackwardPageBtn.Visibility = Visibility.Hidden;
                    this.ForwardPageBtn.Visibility = Visibility.Visible;
                    this.ForwardPageBtn.Content = "开始";
                    break;
                case PageState.MidPage:
                    this.ForwardPageBtn.Visibility = Visibility.Visible;
                    this.BackwardPageBtn.Visibility = Visibility.Visible;
                    this.ForwardPageBtn.Content = "下一步";
                    break;
                case PageState.LastPage:
                    this.BackwardPageBtn.Visibility = Visibility.Visible;
                    this.ForwardPageBtn.Visibility = Visibility.Hidden;
                    this.ForwardPageBtn.Content = "完成";
                    break;
                default:
                    break;
            }
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            if (wizardFrame.NavigationService.CanGoForward)
            {
                wizardFrame.NavigationService.GoForward(); // 导航到下一页
                if (!wizardFrame.NavigationService.CanGoForward)
                    ButtonStateChange(((PageInfo)CurrentPage.Tag).PageState);
                else
                    ButtonStateChange(PageState.MidPage);
                return;
            }

            try
            {
                CurrentPage = PageJumpController.NextPage(CurrentPage);
                wizardFrame.Navigate(CurrentPage);
                ButtonStateChange(((PageInfo)CurrentPage.Tag).PageState);
                //if (ForwardPageBtn.Content.ToString() == "完成")
                //    return;

            }
            catch (Exception ex)
            {

            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (wizardFrame.NavigationService.CanGoBack)
            {
                this.ForwardPageBtn.IsEnabled = true;
                wizardFrame.NavigationService.GoBack(); // 导航到上一页
                if (wizardFrame.NavigationService.CanGoBack)
                    ButtonStateChange(PageState.MidPage);
                else
                    ButtonStateChange(PageState.BeginPage);
                return;
            }
        }

        // Navigated事件处理程序
        private void NavigationService_Navigated(object sender, NavigationEventArgs e)
        {
            // 页面导航完成后的逻辑处理
            // 例如，更新页面状态、执行特定操作等

        }

    }
}
