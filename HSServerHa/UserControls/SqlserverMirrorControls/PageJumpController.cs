﻿using HsServerHa.Entity;
using HsServerHa.UserControls.SqlserverMirrorControls.CheckConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace HsServerHa.UserControls.SqlserverMirrorControls
{
    /// <summary>
    /// 页面信息
    /// </summary>
    public class PageInfo
    {
        public PageInfo(PageState pageState,int PageIndex) { 
            this.PageState = pageState;
            this.PageIndex = PageIndex;
        }

        public PageState PageState { get; set; }

        public int PageIndex;

    }

    /// <summary>
    /// 页面跳转控制
    /// </summary>
    public class PageJumpController
    {
        public PageJumpController()
        {

        }


        public static Page NextPage(Page page)
        {
            try
            {
                PageInfo pageInfo = (PageInfo)page.Tag;
                switch (pageInfo.PageIndex)
                {
                    case 0:
                        return new ParameterSettings();
                    case 1:
                        return new CheckConfiguration();
                    case 2:
                        return new CommandProcessings.CommandProcessing();
                }
            }
            catch (FormatException ex)
            {

            }
            

            return page;
        }
    }
}
