﻿using CommunityToolkit.Mvvm.Messaging.Messages;
using CommunityToolkit.Mvvm.Messaging;
using HsServerHa.Entity;
using HsServerHa.Entity.Models.SqlserverMirror;
using HsServerHa.ViewModel;
using HsServerHa.ViewModel.SqlserverMirrorViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HsServerHa.UserControls.SqlserverMirrorControls
{
    /// <summary>
    /// ParameterSettings.xaml 的交互逻辑
    /// </summary>
    public partial class ParameterSettings : Page
    {
        public ParameterSettings()
        {
            InitializeComponent();
            //ParameterSettingsViewModel parameterSettingsViewModel = new ParameterSettingsViewModel(); 
            //this.DataContext = parameterSettingsViewModel;
            LeftConnection.ServerName = "主服务器";
            LeftConnection.FontBold = "Bold";
            RightConnection.ServerName = "从服务器";            
            Tag = new PageInfo(PageState.MidPage, 1);
            OperateCommandBases = new ObservableCollection<OperateCommandBase>();
            LeftDataBaseEntitys = new List<DataBaseEntity>();
            RightDataBaseEntitys = new List<DataBaseEntity>();
            dbList_DataGrid.DataContext = OperateCommandBases;
            //NavigationService.Navigated += NavigationService_Navigated;
            WeakReferenceMessenger.Default.Send(new ValueChangedMessage<string>("禁用"), "NextButtonIsEnable");
           


        }

        

        public static ObservableCollection<OperateCommandBase> OperateCommandBases
        {
            get; set;
        }


        public static string LeftDataBaseConnection { get; set; }

        public static string LeftServer { get; set; }

        public static string RightServer { get; set; }

        public static string RightDataBaseConnection { get; set; }

        public static string LeftPort { get; set; }

        public static string RightPort { get; set; }

        public static List<DataBaseEntity> LeftDataBaseEntitys { get;set; }

        public static List<DataBaseEntity> RightDataBaseEntitys { get;set; }

        public static string FolderPath;

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (ParameterSettings.OperateCommandBases == null) return;
            // 页面导航完成后的逻辑处理
            // 例如，更新页面状态、执行特定操作等
            if (ParameterSettings.OperateCommandBases.Count > 0)
            {
                WeakReferenceMessenger.Default.Send(new ValueChangedMessage<string>("启用"), "NextButtonIsEnable");
            }
            else
            {
                WeakReferenceMessenger.Default.Send(new ValueChangedMessage<string>("禁用"), "NextButtonIsEnable");
            }
        }
    }

}
