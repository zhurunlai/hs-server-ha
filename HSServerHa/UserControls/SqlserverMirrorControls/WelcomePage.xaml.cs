﻿using HsServerHa.Entity;
using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace HsServerHa.UserControls.SqlserverMirrorControls
{
    /// <summary>
    /// WelcomePage.xaml 的交互逻辑
    /// </summary>
    public partial class WelcomePage : Page
    {
        public WelcomePage()
        {
            InitializeComponent();

            Tag = new PageInfo(PageState.BeginPage, 0);

        }

    }
}
