﻿using HsServerHa.Entity;
using HsServerHa.ViewModel.SqlserverMirrorViewModel;
using System;
using System.Windows.Controls;

namespace HsServerHa.UserControls.SqlserverMirrorControls.CommandProcessings
{
    /// <summary>
    /// FileProcessing.xaml 的交互逻辑
    /// </summary>
    public partial class CommandProcessing : Page
    {
        public CommandProcessing()
        {
            InitializeComponent();
            Tag = new PageInfo(PageState.LastPage, 3);
            this.DataContext = new CommandProcessingViewModel();
        }

    }
}
