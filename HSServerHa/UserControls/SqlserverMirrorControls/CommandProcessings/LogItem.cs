﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace HsServerHa.UserControls.SqlserverMirrorControls.CommandProcessings
{
    public class LogItem
    {
        public string ShowText { get; set; }
        public Brush ShowColor { get; set; }
        public int FontSize { get; set; }
    }
}
