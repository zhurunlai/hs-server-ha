﻿using AduSkin.Controls.Metro;
using CommunityToolkit.Mvvm.Messaging;
using HsServerHa.Common.Auth;
using HsServerHa.Entity.Models;
using HsServerHa.Utility.DataHelper;
using HsServerHa.Utility.NetWork;
using HsServerHa.Utility.SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace HsServerHa.UserControls
{
    /// <summary>
    /// ClusterInfo.xaml 的交互逻辑
    /// </summary>
    public partial class ClusterInfo : UserControl
    {
        private  DispatcherTimer timer;
        public ClusterInfo()
        {
            InitializeComponent();
            #region 授权验证
            if (!AuthorizationVerification.IsVerification())
            {
                AduMessageBoxs.Show("授权到期！");
                return  ;
            }

            #endregion
            WeakReferenceMessenger.Default.Register<string, string>(this, "MenuData", (r, m) =>
            {
                if (m!= "ClusterInfo")
                {
                    if (timer!=null)
                    {
                        timer.Stop();
                        timer = null;
                    }
                   
                }
            });
            initData(null, null);
            if (timer == null)
            {
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(5);
                timer.Tick += initData;
                timer.Start();
            }
           
        }

        public async void initData(object sender, EventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (sender, e) =>
            {
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (App.isMaster != null)
                    {
                        //查询绑定信息  
                        List<MasterSlaveBindEntity> listBind = SQLiteHelper.Table<MasterSlaveBindEntity>().ToList();
                        //List<MasterSlaveBindEntity> listBind = DataHelpers.ConvertToModel<MasterSlaveBindEntity>(DbHelperSQLite.Query("select * from MasterSlaveBind")?.Tables?[0]);
                        var firstBind = listBind.FirstOrDefault();

                        //List<HeartEntity> listHeart = DataHelpers.ConvertToModel<HeartEntity>(DbHelperSQLite.Query("select * from HeartManage")?.Tables?[0]);
                        List<HeartEntity> listHeart = SQLiteHelper.Table<HeartEntity>().ToList();
                        var firstHeart = listHeart.FirstOrDefault();

                        //List<VirtuallyManageEntity> listVir = DataHelpers.ConvertToModel<VirtuallyManageEntity>(DbHelperSQLite.Query("select * from VirtuallyManage")?.Tables?[0]);
                        List<VirtuallyManageEntity> listVir = SQLiteHelper.Table<VirtuallyManageEntity>().ToList();
                        var firstVir = listBind.FirstOrDefault();

                        //查询子虚拟IP
                        //List<MultipleNetworkCardManageEntity> listMutip = DataHelpers.ConvertToModel<MultipleNetworkCardManageEntity>(DbHelperSQLite.Query("select * from MultipleNetworkCardManage where IsEnable='是'")?.Tables?[0]);
                        List<MultipleNetworkCardManageEntity> listMutip = SQLiteHelper.Table<MultipleNetworkCardManageEntity>().Where(x => x.IsEnable == "是").ToList();
                        var firstMutip = listMutip?.FirstOrDefault();
                        if (firstBind == null || firstHeart == null || firstVir == null)
                        {
                            return;
                        }

                        this.zj_IP.Content = firstBind.MasterIP;
                        this.cj_IP.Content = firstBind.SlaveIP;
                        this.xn_ip.Content = firstVir.VirtuallyIP;

                        if (App.isMaster.Value)
                        {
                            this.zj_img.Visibility = Visibility.Visible;
                            this.cj_img.Visibility = Visibility.Hidden;

                            this.zj_xt_grid.Background = Brushes.Green;
                            this.zj_ck_name.Content = "正常";
                            if (App.IsPingRefer)
                            {
                                this.zj_ck_grid.Background = Brushes.Green;
                                this.cj_ck_name.Content = "正常";
                            }
                            else
                            {
                                this.zj_ck_grid.Background = Brushes.Red;
                                this.cj_ck_name.Content = "异常";
                            }
                            this.zj_xt_name.Content = "正常";

                            if (App.XtIsOpen)
                            {
                                if (NetWorkHelper.IsPingIP(firstBind.SlaveIP))
                                {
                                    this.cj_ck_grid.Background = Brushes.Green;
                                    this.cj_xt_grid.Background = Brushes.Green;
                                    this.cj_xt_name.Content = "正常";
                                }
                                else
                                {
                                    this.cj_ck_grid.Background = Brushes.Red;
                                    this.cj_xt_grid.Background = Brushes.Red;
                                    this.cj_xt_name.Content = "异常";
                                }

                            }
                            else
                            {
                                this.cj_ck_grid.Background = Brushes.Red;
                                this.cj_xt_grid.Background = Brushes.Red;
                                this.cj_xt_name.Content = "异常";
                                this.cj_ck_name.Content = "异常";
                            }

                            //ping虚拟IP
                            if (NetWorkHelper.IsPingIP(firstVir.VirtuallyIP))
                            {
                                this.xn_grid.Background = Brushes.Green;
                                this.xn_name.Content = "正常";
                            }
                            else
                            {
                                this.xn_grid.Background = Brushes.Red;
                                this.xn_name.Content = "异常";
                            }

                            #region 子虚拟主机
                            //this.zj_xn_panel.DataContext;
                            if (firstMutip == null)
                            {
                                this.ChildVirtually.Visibility = Visibility.Hidden;
                            }
                            else
                            {
                                this.ChildVirtually.Visibility = Visibility.Visible;
                                this.ChildVirtually.Visibility = Visibility.Visible;
                                double height = 118 / (listMutip.Count() == 0 ? 1 : listMutip.Count());
                                this.zj_xn_panel.Children.Clear();
                                foreach (var item in listMutip)
                                {
                                    var isPing = false;
                                    if (NetWorkHelper.IsPingIP(item.CurrIP) && NetWorkHelper.IsPingIP(item.VirtuallyIP))
                                    {
                                        isPing = true;
                                    }
                                    WrapPanel wrapPanel = new WrapPanel();
                                    wrapPanel.Width = 300;
                                    wrapPanel.Background = isPing ? Brushes.Green : Brushes.Red;
                                    wrapPanel.VerticalAlignment = VerticalAlignment.Center;
                                    wrapPanel.Height = height;
                                    Label label1 = new Label();
                                    label1.Margin = new Thickness(10, 0, 0, 0);
                                    label1.Content = "主机";
                                    label1.Foreground = Brushes.White;
                                    label1.VerticalAlignment = VerticalAlignment.Center;
                                    label1.VerticalContentAlignment = VerticalAlignment.Center;
                                    label1.Height = height;
                                    wrapPanel.Children.Add(label1);
                                    Label label2 = new Label();
                                    label2.Margin = new Thickness(0, 0, 0, 0);
                                    label2.Content = item.CurrIP;
                                    label2.Foreground = Brushes.White;
                                    label2.VerticalAlignment = VerticalAlignment.Center;
                                    label2.VerticalContentAlignment = VerticalAlignment.Center;
                                    label2.Height = height;
                                    wrapPanel.Children.Add(label2);

                                    Label label3 = new Label();
                                    label3.Margin = new Thickness(0, 0, 0, 0);
                                    label3.Content = "虚拟IP";
                                    label3.Foreground = Brushes.White;
                                    label3.VerticalAlignment = VerticalAlignment.Center;
                                    label3.VerticalContentAlignment = VerticalAlignment.Center;
                                    label3.Height = height;
                                    wrapPanel.Children.Add(label3);

                                    Label label4 = new Label();
                                    label4.Margin = new Thickness(0, 0, 0, 0);
                                    //label4.Content = "192.168.180.172";// item.VirtuallyIP;
                                    label4.Content = firstMutip.VirtuallyIP;
                                    label4.Foreground = Brushes.White;
                                    label4.VerticalAlignment = VerticalAlignment.Center;
                                    label4.VerticalContentAlignment = VerticalAlignment.Center;
                                    label4.Height = height;
                                    wrapPanel.Children.Add(label4);


                                    this.zj_xn_panel.Children.Add(wrapPanel);
                                }

                                //从 
                                this.cj_cxn_grid.Children.Clear();
                                foreach (var item in listMutip)
                                {
                                    var isPing = false;
                                    if (NetWorkHelper.IsPingIP(item.RemoteIP))
                                    {
                                        isPing = true;
                                    }
                                    WrapPanel wrapPanel = new WrapPanel();
                                    wrapPanel.Background = isPing ? Brushes.Green : Brushes.Red;
                                    wrapPanel.VerticalAlignment = VerticalAlignment.Center;
                                    wrapPanel.Height = height;
                                    Label label1 = new Label();
                                    label1.Margin = new Thickness(10, 0, 0, 0);
                                    label1.Content = "从机";
                                    label1.Foreground = Brushes.White;
                                    label1.VerticalAlignment = VerticalAlignment.Center;
                                    label1.VerticalContentAlignment = VerticalAlignment.Center;
                                    label1.Height = height;
                                    wrapPanel.Children.Add(label1);

                                    Label label2 = new Label();
                                    label2.Margin = new Thickness(10, 0, 0, 0);
                                    label2.Content = item.RemoteIP;
                                    label2.Foreground = Brushes.White;
                                    label2.VerticalAlignment = VerticalAlignment.Center;
                                    label2.VerticalContentAlignment = VerticalAlignment.Center;
                                    label2.Height = height;
                                    wrapPanel.Children.Add(label2);
                                    this.cj_cxn_grid.Children.Add(wrapPanel);
                                }

                            }
                            #endregion


                        }
                        else
                        {
                            this.zj_img.Visibility = Visibility.Hidden;
                            this.cj_img.Visibility = Visibility.Visible;

                            this.cj_xt_grid.Background = Brushes.Green;
                            this.cj_xt_name.Content = "正常";
                            this.cj_ck_name.Content = "正常";
                            if (App.IsPingRefer)
                            {
                                this.cj_ck_grid.Background = Brushes.Green;
                                this.zj_ck_name.Content = "正常";
                            }
                            else
                            {
                                this.cj_ck_grid.Background = Brushes.Red;
                                this.zj_ck_name.Content = "异常";
                            }
                            if (App.XtIsOpen)
                            {

                                this.zj_xt_name.Content = "正常";
                                this.zj_xt_grid.Background = Brushes.Green;

                                this.zj_ck_grid.Background = Brushes.Green;
                            }
                            else
                            {
                                this.zj_xt_name.Content = "异常";
                                this.zj_ck_grid.Background = Brushes.Red;
                                this.zj_xt_grid.Background = Brushes.Red;
                            }

                            //ping虚拟IP
                            if (NetWorkHelper.IsPingIP(firstVir.VirtuallyIP))
                            {
                                this.xn_grid.Background = Brushes.Green;
                                this.xn_name.Content = "正常";
                            }
                            else
                            {
                                this.xn_grid.Background = Brushes.Red;
                                this.xn_name.Content = "异常";
                            }

                            #region 子虚拟主机
                            //this.zj_xn_panel.DataContext;
                            if (firstMutip == null)
                            {
                                this.ChildVirtually.Visibility = Visibility.Hidden;
                            }
                            else
                            {
                                //从
                                this.ChildVirtually.Visibility = Visibility.Visible;
                                double height = 118 / (listMutip.Count() == 0 ? 1 : listMutip.Count());
                                this.cj_cxn_grid.Children.Clear();
                                foreach (var item in listMutip)
                                {
                                    var isPing = false;
                                    if (NetWorkHelper.IsPingIP(item.CurrIP))
                                    {
                                        isPing = true;
                                    }
                                    WrapPanel wrapPanel = new WrapPanel();
                                    wrapPanel.Background = isPing ? Brushes.Green : Brushes.Red;
                                    wrapPanel.VerticalAlignment = VerticalAlignment.Center;
                                    wrapPanel.Height = height;
                                    Label label1 = new Label();
                                    label1.Margin = new Thickness(10, 0, 0, 0);
                                    label1.Content = "从机";
                                    label1.Foreground = Brushes.White;
                                    label1.VerticalAlignment = VerticalAlignment.Center;
                                    label1.VerticalContentAlignment = VerticalAlignment.Center;
                                    label1.Height = height;
                                    wrapPanel.Children.Add(label1);

                                    Label label2 = new Label();
                                    label2.Margin = new Thickness(10, 0, 0, 0);
                                    label2.Content = item.CurrIP;
                                    label2.Foreground = Brushes.White;
                                    label2.VerticalAlignment = VerticalAlignment.Center;
                                    label2.VerticalContentAlignment = VerticalAlignment.Center;
                                    label2.Height = height;
                                    wrapPanel.Children.Add(label2);
                                    this.cj_cxn_grid.Children.Add(wrapPanel);
                                }


                                //主
                                this.ChildVirtually.Visibility = Visibility.Visible;
                                //double height = 118 / (listMutip.Count() == 0 ? 1 : listMutip.Count());
                                this.zj_cxn_grid.Children.Clear();
                                foreach (var item in listMutip)
                                {
                                    var isPing = false;
                                    if (NetWorkHelper.IsPingIP(item.RemoteIP))
                                    {
                                        isPing = true;
                                    }
                                    WrapPanel wrapPanel = new WrapPanel();
                                    wrapPanel.Background = isPing ? Brushes.Green : Brushes.Red;
                                    wrapPanel.VerticalAlignment = VerticalAlignment.Center;
                                    wrapPanel.Height = height;
                                    Label label1 = new Label();
                                    label1.Margin = new Thickness(10, 0, 0, 0);
                                    label1.Content = "主机";
                                    label1.Foreground = Brushes.White;
                                    label1.VerticalAlignment = VerticalAlignment.Center;
                                    label1.VerticalContentAlignment = VerticalAlignment.Center;
                                    label1.Height = height;
                                    wrapPanel.Children.Add(label1);

                                    Label label2 = new Label();
                                    label2.Margin = new Thickness(0, 0, 0, 0);
                                    label2.Content = item.RemoteIP;
                                    label2.Foreground = Brushes.White;
                                    label2.VerticalAlignment = VerticalAlignment.Center;
                                    label2.VerticalContentAlignment = VerticalAlignment.Center;
                                    label2.Height = height;
                                    wrapPanel.Children.Add(label2);

                                    Label label3 = new Label();
                                    label3.Margin = new Thickness(10, 0, 0, 0);
                                    label3.Content = "虚拟IP";
                                    label3.Foreground = Brushes.White;
                                    label3.VerticalAlignment = VerticalAlignment.Center;
                                    label3.VerticalContentAlignment = VerticalAlignment.Center;
                                    label3.Height = height;
                                    wrapPanel.Children.Add(label3);

                                    Label label4 = new Label();
                                    label4.Margin = new Thickness(0, 0, 0, 0);
                                    label4.Content = item.VirtuallyIP;
                                    label4.Foreground = Brushes.White;
                                    label4.VerticalAlignment = VerticalAlignment.Center;
                                    label4.VerticalContentAlignment = VerticalAlignment.Center;
                                    label4.Height = height;
                                    wrapPanel.Children.Add(label4);

                                    this.zj_cxn_grid.Children.Add(wrapPanel);
                                }
                            }
                            #endregion
                        }



                    }

                }), null);
            };
            worker.RunWorkerAsync();
          

        }

        protected override void OnContentChanged(object a,object b) {
        
        }
        //protected override void OnVisualParentChanged(DependencyObject oldParent)
        //{
        //    try
        //    {
        //        if (oldParent != null)
        //        {
        //            //MyOwnDisposeMethod(); //Release all resources here 
        //            WeakReferenceMessenger.Default.Unregister<string, string>(this, "MenuData");
        //            timer.Stop();
        //            timer = null;
        //        }


        //        base.OnVisualParentChanged(oldParent);

        //    }
        //    catch (Exception ex)
        //    {
        //        WeakReferenceMessenger.Default.Unregister<string, string>(this, "MenuData");
        //        timer.Stop();
        //        timer = null;
        //    }
         
        //}
    }
}
