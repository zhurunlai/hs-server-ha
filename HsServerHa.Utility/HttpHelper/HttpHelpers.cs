﻿using HsServerHa.Core.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Utility.HttpHelper
{
    public class HttpHelpers
    {
        public static WebClient webClient;
        public HttpHelpers()
        {

        }
        /// <summary>
        /// get请求
        /// 朱润来
        /// 2022年6月16日14:08:50
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static async Task<List<T>> GetList<T>(string path)
        {
            try
            {
                if (webClient == null)
                {
                    webClient = new WebClientBase();
                }
                //以form表单的形式上传
                webClient.Headers.Add("Content-Type", "application/json");
                // 转化成二进制数组
                var stream = webClient.OpenRead(path);
                //获取返回的二进制数据
                using (StreamReader reader = new StreamReader(stream))
                {
                    var str = (reader.ReadToEnd());
                    return JsonConvert.DeserializeObject<List<T>>(str);
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message+ex.ToString());
                return null;
            }

        }
        public static async Task<T> Get<T>(string path)
        {
            try
            {
                if (webClient == null)
                {
                    webClient = new WebClientBase();
                }
                //以form表单的形式上传
                webClient.Headers.Add("Content-Type", "application/json");
                // 转化成二进制数组
                var stream = webClient.OpenRead(path);
                //获取返回的二进制数据
                using (StreamReader reader = new StreamReader(stream))
                {
                    var str = (reader.ReadToEnd());
                    return JsonConvert.DeserializeObject<T>(str);
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(new HttpResponseResultModel<object>() { IsSuccess = false, ErrorMessage = ex.Message }));
            }

        }

        /// <summary>
        /// post请求
        /// 朱润来
        /// 2022年6月16日14:09:08
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async Task<T> PostToJson<T>(string path, string param = null)
        {
            try
            {
                if (webClient == null)
                {
                    webClient = new WebClientBase();
                }
                //以form表单的形式上传
                webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                // 转化成二进制数组
                if (param == null)
                {
                    param = "";
                }
                byte[] postData = Encoding.UTF8.GetBytes(param);
                // 上传数据
                byte[] responseData = webClient.UploadData(path, "post", postData);
                //获取返回的二进制数据
                string result = Encoding.UTF8.GetString(responseData);
                return JsonConvert.DeserializeObject<T>(result);
            }
            catch (Exception ex)
            {
                return default(T);
            }

        }

        /// <summary>
        /// post请求
        /// 朱润来
        /// 2022年6月16日14:09:08
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async Task<T> Post<T>(string path, string param = null)
        {
            try
            {
                if (webClient == null)
                {
                    webClient = new WebClientBase();
                } 
                webClient.Headers.Add("Content-Type", "application/json");
                // 转化成二进制数组
                if (param == null)
                {
                    param = "";
                }
                byte[] postData = Encoding.UTF8.GetBytes(param);
                // 上传数据
                byte[] responseData = webClient.UploadData(path, "post", postData);
                //获取返回的二进制数据
                string result = Encoding.UTF8.GetString(responseData);
                return JsonConvert.DeserializeObject<T>(result);
            }
            catch (Exception ex)
            {
                return default(T);
            }

        }

        public static async Task<T> Del<T>(string path, string param = null)
        {
            try
            {
                if (webClient == null)
                {
                    webClient = new WebClientBase();
                }
                //Uri _uri = new Uri(path);
                ////以form表单的形式上传
                //webClient.Headers.Add("Content-Type", "application/json-patch+json");
                //// 转化成二进制数组
                //byte[] postData = Encoding.UTF8.GetBytes(param);
                //// 上传数据
                //webClient.UploadDataAsync(_uri, "DELETE", postData);
                ////获取返回的二进制数据
                ////string result = Encoding.UTF8.GetString(responseData);

                //return true;

                //Uri collectionUri = new Uri(path);

                //Uri requestUri;
                //requestUri = new Uri(collectionUri, param);
                //Console.WriteLine("DELETE " + requestUri);
                //HttpClient client = new HttpClient();
                //var response = await client.DeleteAsync(requestUri);
                //return JsonConvert.DeserializeObject<T>(response.RequestMessage.ToString());

                //以form表单的形式上传
                webClient.Headers.Add("Content-Type", "application/json");
                // 转化成二进制数组
                byte[] postData = Encoding.UTF8.GetBytes(param);
                // 上传数据
                byte[] responseData = webClient.UploadData(path, "DELETE", postData);
                //获取返回的二进制数据
                string result = Encoding.UTF8.GetString(responseData);
                return JsonConvert.DeserializeObject<T>(result);
            }
            catch (Exception ex)
            {

                return default(T);
            }
        }
    }
}
