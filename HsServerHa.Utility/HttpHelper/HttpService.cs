﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Utility.HttpHelper
{
    public class HttpService
    {
        private static HttpService instance;
        private static HttpClient client;

        private HttpService()
        {
            client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(5); // 设置超时时间为30秒
        }

        public static HttpService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new HttpService();
                }
                return instance;
            }
        }

        public async Task<string> Get(string url)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (OperationCanceledException)
            {
                // 处理超时异常
                Console.WriteLine("Request timed out");
                return null;
            }
            catch (HttpRequestException e)
            {
                // 处理请求异常
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public async Task<string> Post(string url, string data)
        {
            try
            {
                var content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(url, content).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (OperationCanceledException)
            {
                // 处理超时异常
                Console.WriteLine("Request timed out");
                return null;
            }
            catch (HttpRequestException e)
            {
                // 处理请求异常
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public async Task<string> Delete(string url)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(url);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (OperationCanceledException)
            {
                // 处理超时异常
                Console.WriteLine("Request timed out");
                return null;
            }
            catch (HttpRequestException e)
            {
                // 处理请求异常
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public async Task<string> Put(string url, string data)
        {
            try
            {
                var content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(url, content);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (OperationCanceledException)
            {
                // 处理超时异常
                Console.WriteLine("Request timed out");
                return null;
            }
            catch (HttpRequestException e)
            {
                // 处理请求异常
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
