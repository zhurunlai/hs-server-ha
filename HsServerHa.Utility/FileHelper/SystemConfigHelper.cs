﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Utility.FileHelper
{
    public class SystemConfigHelper
    {
        /// <summary>
        /// 获取系统配置文件
        /// </summary>
        /// <returns></returns>
        public static string GetSystemConfig()
        {
            string json = "";
            try
            {
                json = File.ReadAllText("Config\\SystemConfig.json");

            }
            catch (Exception ex)
            {
                LoggerHelper.Error($"获取配置报错:{ex.Message}");
            }
            return json;

        }
        /// <summary>
        /// 写配置文件
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static void WriteSystemConfig(string json)
        {
            try
            {
                File.WriteAllText("Config\\SystemConfig.json", json);

            }
            catch (Exception ex)
            {
                LoggerHelper.Error($"写SystemConfig失败:{ex.Message}");
            }


        }
    }
}
