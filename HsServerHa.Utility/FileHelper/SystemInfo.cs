﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Utility.FileHelper
{  

    /// <summary>
    /// 系统配置信息
    /// </summary>
    public class SystemInfo
    {
        /// <summary>
        /// 切换主机时间戳
        /// </summary>
        public long SwitchHostTimeStamp { get; set; }

        /// <summary>
        /// 切换主机时间
        /// </summary>
        public string SwitchHostTime { get; set; }
    }

}
