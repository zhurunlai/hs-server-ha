﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Utility.FileHelper
{
    public class FileTransferClient
    {
        /// <summary>
        /// 传输文件
        /// </summary>
        /// <param name="serverIp"></param>
        /// <param name="port"></param>
        /// <param name="sourcePath"></param>
        public static async Task<bool> TransferFileAsync(string serverIp, int port, string sourcePath)
        {
            bool result = true;
            try
            {
                using (TcpClient fileClient = new TcpClient())
                {
                    await fileClient.ConnectAsync(serverIp, port); // 异步连接
                    using (NetworkStream fileNetworkStream = fileClient.GetStream())
                    {
                        using (FileStream fileStream = File.OpenRead(sourcePath))
                        {
                            byte[] buffer = new byte[8096];
                            int bytesRead;
                            while ((bytesRead = await fileStream.ReadAsync(buffer, 0, buffer.Length)) > 0) // 异步读取文件
                            {
                                await fileNetworkStream.WriteAsync(buffer, 0, bytesRead); // 异步写入网络流
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                LoggerHelper.Error(ex.ToString(), "文件传输");
            }
            return result;
        }
        public static bool TransferFile(string serverIp, int port, string sourcePath)
        {
            bool result = true;
            try
            {
                using (TcpClient fileClient = new TcpClient())
                {
                    fileClient.Connect(serverIp, port);
                    NetworkStream fileNetworkStream = fileClient.GetStream();
                    using (FileStream fileStream = File.OpenRead(sourcePath))
                    {
                        byte[] buffer = new byte[8096];
                        int bytesRead;
                        while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            fileNetworkStream.Write(buffer, 0, bytesRead);
                        }
                    }
                    fileNetworkStream.Close();
                    fileClient.Close();
                }
            }
            catch (Exception ex)
            {
                result = false;
                LoggerHelper.Error(ex.ToString(), "文件传输");
            }
            return result;
        }
        /// <summary>
        /// 传输消息
        /// </summary>
        /// <param name="serverIp"></param>
        /// <param name="port"></param>
        public static string TransferMessage(string serverIp, int port)
        {
            string msg = "";
            try
            {
                using (TcpClient messageClient = new TcpClient())
                {
                    messageClient.Connect(serverIp, port);
                    using (NetworkStream messageNetworkStream = messageClient.GetStream())
                    {
                        // 读取确认消息
                        byte[] buffer = new byte[1024];
                        int bytesRead = messageNetworkStream.Read(buffer, 0, buffer.Length);
                        msg = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                        messageNetworkStream.Close();
                    }                   
                   messageClient.Close();
                }
            }
            catch(Exception ex)
            {
                msg = ex.Message;
            }
            return msg;
        }
    }
}
