﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Utility.FileHelper
{
    public class FileHelpers
    {
        public static bool IsExist(string path, string fileName)
        {
            string fullName = path + fileName;
            if (Directory.Exists(path))
            {
                return File.Exists(fullName);
            }
            else
                return false;

        }

        public static long Size(string path, string fileName)
        {
            string fullName = path + fileName;
            if (!File.Exists(fullName)) return 0;
            // 通过构造函数实例化 FileInfo 对象
            FileInfo fileInfo = new FileInfo(fullName);
            return fileInfo.Length;

        }

        /// <summary>
        /// 创建文件路径
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool CreateDic(string path)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch
                {
                    return false;
                }
                return true;
            }



            
        }
    }
}