﻿using System.Reflection;

namespace HsServerHa.Utility
{
    public class UtilityAssembly
    {
        /// <summary>
        /// 加载程序集
        /// </summary>
        /// <returns></returns>
        public static Assembly GetAssembly()
        {
            return Assembly.GetExecutingAssembly();
        }
    }
}
