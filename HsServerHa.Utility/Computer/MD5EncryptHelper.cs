﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HsServerHa.Utility.Computer
{
    public class MD5EncryptHelper
    {
        //第一种：由于MD5是不可逆的，所以加密之后就无法解密，取用户名和密码时候，
        //需要再加密一边用户输入的数据与数据库中已加密的数据进行比对。如果比对结果一致，则可以判定登陆成功！
        #region 16位MD5加密
        /// <summary>
        /// 16位MD5加密，无法解密
        /// </summary>
        /// <param name="password">密码</param>
        /// <returns>加密后的密码</returns>
        public static string MD5Encrypt16(string password)
        {
            var md5 = new MD5CryptoServiceProvider();
            string t2 = BitConverter.ToString(md5.ComputeHash(Encoding.Default.GetBytes(password)), 4, 8);
            t2 = t2.Replace("-", "");
            return t2;
        }
        #endregion

        #region 32位MD5加密
        /// <summary>
        /// 32位MD5加密，无法解密
        /// </summary>
        /// <param name="password">密码</param>
        /// <returns>加密后的密码</returns>
        public static string MD5Encrypt32(string password)
        {
            string cl = password;
            string pwd = "";
            MD5 md5 = MD5.Create(); //实例化一个md5对像
            // 加密后是一个字节类型的数组，这里要注意编码UTF8/Unicode等的选择　
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(cl));
            // 通过使用循环，将字节类型的数组转换为字符串，此字符串是常规字符格式化所得
            for (int i = 0; i < s.Length; i++)
            {
                // 将得到的字符串使用十六进制类型格式。格式后的字符是小写的字母，如果使用大写（X）则格式后的字符是大写字符 
                pwd = pwd + s[i].ToString("X");
            }
            return pwd;
        }
        #endregion
    }
}
