﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace HsServerHa.Utility.Computer
{
    public class ComputerHelper
    {


        /// <summary>
        /// 服务器机器码信息
        /// </summary>
        public static string ComputerInfos { get; set; }
        public static string ComputerInfo()
        {

            if (string.IsNullOrEmpty(ComputerInfos))
            {
                //Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss,fffff") + "   a");
                string info = string.Empty;
                string cpu = GetCPUInfo();
                string baseBoard = GetBaseBoardInfo();
                string bios = GetBIOSInfo().Replace(" ", "");
                string mac = GetMACInfo();
                //info = string.Concat(cpu, baseBoard, bios, mac);
                info = string.Concat(cpu, baseBoard, bios);
                ComputerInfos = info;
                //Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss,fffff" + "   b"));
                return info;
            }
            else
            {
                return ComputerInfos;
            }
            
        }

        private static string GetCPUInfo()
        {
            string info = string.Empty;
            info = GetHardWareInfo("Win32_Processor", "ProcessorId");
            return info;
        }

        private static string GetBaseBoardInfo()
        {
            string info = string.Empty;
            info = GetHardWareInfo("Win32_BaseBoard", "SerialNumber");
            return info;
        }

        private static string GetBIOSInfo()
        {
            string info = string.Empty;
            info = GetHardWareInfo("Win32_BIOS", "SerialNumber");
            return info;
        }

        private static string GetMACInfo()
        {
            string info = string.Empty;
            info = "";
            return info;
        }

        private static string GetHardWareInfo(string typePath, string Key)
        {
            try
            {
                ManagementClass mageClass = new ManagementClass(typePath);
                ManagementObjectCollection mageObjectColl = mageClass.GetInstances();
                PropertyDataCollection Properties = mageClass.Properties;
                foreach (PropertyData property in Properties)
                {
                    if (property.Name == Key)
                    {
                        foreach (ManagementObject mageObject in mageObjectColl)
                        {
                            return mageObject.Properties[property.Name].Value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return string.Empty;
        }
         
    }
}
