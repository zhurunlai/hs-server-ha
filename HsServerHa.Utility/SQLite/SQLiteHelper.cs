﻿using HsServerHa.Entity.Models;
using Microsoft.VisualBasic;
using SQLite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace HsServerHa.Utility.SQLite
{
    public class SQLiteHelper
    {
        public static string connstr = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["dataFile"].ToString();
        public static SQLiteConnection db;
        public static SQLiteAsyncConnection dbAsync;
        public SQLiteHelper()
        {
            NewDbFile(connstr);//创建数据库文件

            db = new SQLiteConnection(connstr, false);
            dbAsync = new SQLiteAsyncConnection(connstr, false);

            db.CreateTable<HeartEntity>();//表已存在不会重复创建
            db.CreateTable<LoggerManageEntity>();
            db.CreateTable<MasterSlaveBindEntity>();
            db.CreateTable<MultipleNetworkCardManageEntity>();
            db.CreateTable<ProgramManageEntity>();
            db.CreateTable<SQLManageEntity>();
            db.CreateTable<VirtuallyManageEntity>();
            db.CreateTable<FilesSyncManageEntity>();
            db.CreateTable<FilesTargetManageEntity>();
            db.CreateTable<AlarmPushManageEntity>();
            db.CreateTable<DatabaseServerEntity>();
            db.CreateTable<DbBackFilePathEntity>();
        }
        /// <summary>
        /// 新建数据库文件
        /// </summary>
        /// <param name="dbPath">数据库文件路径及名称</param>
        /// <returns>新建成功，返回true，否则返回false</returns>
        void NewDbFile(string dbPath)
        {
            //try
            //{
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("新建数据库文件" + dbPath + "失败：" + ex.Message);
            //}

            string filePathOnly = Path.GetDirectoryName(dbPath);
            //判断文件是否存在
            if (!File.Exists(dbPath))
            {
                //判断文件夹是否存在
                if (!Directory.Exists(filePathOnly))
                {
                    try
                    {
                        Directory.CreateDirectory(filePathOnly);
                        //创建数据库文件
                        System.Data.SQLite.SQLiteConnection.CreateFile(connstr);
                    }
                    catch (Exception ex)
                    {
                        LoggerHelper.Error("新建数据库文件" + dbPath + "失败：" + ex.Message, "SQLite");
                    }
                }
            }
        }


        /// <summary>
        /// 新增
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static int Add(object model)
        {
            try
            {
                return db.Insert(model);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return -1;
            }

        }

        /// <summary>
        /// 新增（异步）
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static async Task<int> AddAsync(object model)
        {
            try
            {
                return await dbAsync.InsertAsync(model);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return -1;
            }

        }

        /// <summary>
        /// 修改
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static int Update(object model)
        {
            try
            {
                return db.Update(model);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return -1;
            }

        }

        /// <summary>
        /// 修改（异步）
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static async Task<int> UpdateAsync(object model)
        {
            try
            {
                return await dbAsync.UpdateAsync(model);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return -1;
            }

        }

        /// <summary>
        /// 删除
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static int Delete(object model)
        {
            try
            {
                return db.Delete(model);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return -1;
            }

        }

        /// <summary>
        /// 删除（异步）
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static async Task<int> DeleteAsync(object model)
        {
            try
            {
                return await dbAsync.DeleteAsync(model);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return -1;
            }

        }

        /// <summary>
        /// 根据SQL查询
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<T> Query<T>(string sql) where T : new()
        {
            try
            {
                return db.Query<T>(sql);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return default;
            }

        }

        /// <summary>
        /// 根据SQL查询（异步）
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static async Task<List<T>> QueryAsync<T>(string sql) where T : new()
        {
            try
            {
                return await dbAsync.QueryAsync<T>(sql);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return default;
            }

        }

        /// <summary>
        /// 执行SQL语句
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static int ExecuteSql(string sql)
        {
            try
            {
                return db.Execute(sql);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return -1;
            }

        }

        /// <summary>
        /// 执行SQL语句（异步）
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static async Task<int> ExecuteSqlAsync(string sql)
        {
            try
            {
                return await dbAsync.ExecuteAsync(sql);
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return -1;
            }

        }

        /// <summary>
        /// 查询单表
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static TableQuery<T> Table<T>() where T : new()
        {
            try
            {
                return db.Table<T>();
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return default;
            }

        }

        /// <summary>
        /// 查询单表（异步）
        /// 朱润来
        /// 2023年11月10日12:56:19
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static AsyncTableQuery<T> TableAsync<T>() where T : new()
        {
            try
            {
                return dbAsync.Table<T>();
            }
            catch (Exception ex)
            {
                LoggerHelper.Error(ex.Message, "SQLite");
                return default;
            }

        }

    }
}
