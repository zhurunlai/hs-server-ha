﻿using HsServerHa.Utility.Computer;
using System;
using System.IO;

namespace HsServerHa.Utility.DataHelper
{
    public class IniFileConfig
    {
        /// <summary>
        /// 当前实列
        /// </summary>
        public readonly static IniFileConfig Current = new IniFileConfig();

        /// <summary>
        /// 当前配置文件名
        /// </summary>
        string _iniFileName = AppDomain.CurrentDomain.BaseDirectory + "Config\\config.ini";

        private IniFileConfig()
        {
            CanCreateFile();
        }

        /// <summary>
        /// 获取的值转换为BOOL
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool GetBoolValue(string section, string key)
        {
            bool result = false;

            string value = GetStringValue(section, key);
            //转换为BOOL
            if (!string.IsNullOrEmpty(value))
            {
                bool.TryParse(value, out result);
            }

            return result;
        }


        /// <summary>
        /// 获取的值转换为BOOL
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public int GetIntValue(string section, string key)
        {
            int result = -1;

            string value = GetStringValue(section, key);
            //转换为BOOL
            if (!string.IsNullOrEmpty(value))
            {
                int.TryParse(value, out result);
            }

            return result;
        }


        /// <summary>
        /// 获取的值转换为BOOL
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetStringValue(string section, string key)
        {
            string result = string.Empty;

            if (File.Exists(_iniFileName))
            {
                result = IniFileAPI.INIGetStringValue(_iniFileName, section, key, "");
            }

            return result;
        }

        /// <summary>
        /// 写入INI配置
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool SetStringValue(string section, string key, string value)
        {
            var result = false;

            if (File.Exists(_iniFileName))
            {
                result = IniFileAPI.INIWriteValue(_iniFileName, section, key, value);
            }

            return result;
        }

        /// <summary>
        /// 创建配置文件
        /// </summary>
        public void CanCreateFile()
        {
            if (!File.Exists(_iniFileName))
            {
                File.Create(_iniFileName);
                //写入一个默认值
            }
            //写入一个默认值
            if (string.IsNullOrEmpty(GetStringValue("UserConfig", "IsMainPC")))
            {
                IniFileAPI.INIWriteValue(_iniFileName, "UserConfig", "IsMainPC", "");
            }

            //写入一个默认值
            if (string.IsNullOrEmpty(GetStringValue("UserConfig", "UserName")))
            {
                IniFileAPI.INIWriteValue(_iniFileName, "UserConfig", "UserName", "admin");
            }

            //写入一个默认值
            if (string.IsNullOrEmpty(GetStringValue("UserConfig", "UserPwd")))
            {
                IniFileAPI.INIWriteValue(_iniFileName, "UserConfig", "UserPwd", "1");
            } 
            //写入一个默认值
            if (string.IsNullOrEmpty(GetStringValue("UserConfig", "PCKey")))
            { 
                var dt = DateTime.Now.ToString("yyyyMMdd");
                var val= RegistryHelper.EncryptString(dt, RegistryHelper.key);
                IniFileAPI.INIWriteValue(_iniFileName, "UserConfig", "PCKey", val);
            }
            //写入一个默认值
            if (string.IsNullOrEmpty(GetStringValue("UserConfig", "Day")))
            {
                IniFileAPI.INIWriteValue(_iniFileName, "UserConfig", "Day", "15");
            }
            //写入一个默认值
            if (string.IsNullOrEmpty(GetStringValue("UserConfig", "IsAutoSwitch")))
            {
                IniFileAPI.INIWriteValue(_iniFileName, "UserConfig", "IsAutoSwitch", "true");
            }

            //写入一个默认值
            if (string.IsNullOrEmpty(GetStringValue("UserConfig", "IsStartDaemon")))
            {
                IniFileAPI.INIWriteValue(_iniFileName, "UserConfig", "IsStartDaemon", "true");
            }  
            //写入一个默认值
            if (string.IsNullOrEmpty(GetStringValue("UserConfig", "IsStartSyncFile")))
            {
                IniFileAPI.INIWriteValue(_iniFileName, "UserConfig", "IsStartSyncFile", "true");
            }



        }

    }
}
