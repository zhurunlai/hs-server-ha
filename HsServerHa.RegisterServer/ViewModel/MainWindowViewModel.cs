﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Messaging;
using HsServerHa.RegisterServer.Common;
using HsServerHa.RegisterServer.Common.Menu;
using HsServerHa.RegisterServer.UserControls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace HsServerHa.RegisterServer.ViewModel
{
    public class MainWindowViewModel: ObservableObject
    {
        public MainWindowViewModel() {
            var json = CustomConfigManager.GetMenuConfig();
            var menu = JsonConvert.DeserializeObject<MenuLabel>(json);

            MenuConfigs = new ObservableCollection<MenuConfig>(menu.MenuData);
            var configsStr = MenuConfigs;
            AppendColor("", configsStr, Brushes.Red);
            MenuConfigs = new ObservableCollection<MenuConfig>(configsStr);
            TabName = "首页";
            WeakReferenceMessenger.Default.Register<string, string>(this, "MenuData", (r, m) =>
            {
                Assembly asm = Assembly.GetExecutingAssembly();
                Type[] types = asm.GetTypes();
                foreach (Type t in types)
                {
                    if (t.Name == m)
                    {
                        if (t.IsClass)
                        {
                            this.CurrentPage = Activator.CreateInstance(t) as UserControl;
                            var configs = MenuConfigs;
                            AppendColor(m, configs, Brushes.Red);
                            MenuConfigs = new ObservableCollection<MenuConfig>(configs);
                        }
                    }
                }
            });
        }

        public void AppendColor(string route, ObservableCollection<MenuConfig> menus, SolidColorBrush brush)
        {
            foreach (var item in menus)
            {
                if (item.ChildNode != null)
                {
                    bool isCheck = false;
                    foreach (var itm in item.ChildNode)
                    {
                        if (route == itm.Route)
                        {
                            //itm.CheckColor = System.Windows.Media.Brushes.Red;
                            itm.CheckColor = brush;// Theme.CurrentColor.OpaqueSolidColorBrush;


                            itm.Title = "    " + itm.Title.Trim();
                            item.IsExpanded = true;
                            isCheck = true;
                        }
                        else
                        {
                            itm.CheckColor = System.Windows.Media.Brushes.Black;
                            itm.Title = "    " + itm.Title.Trim();
                        }
                    }
                    if (isCheck)
                    {
                        item.IsExpanded = true;
                    }
                    else
                    {
                        item.IsExpanded = false;
                    }
                }
                else
                {
                    if (route == item.Route)
                    {
                        item.CheckColor = System.Windows.Media.Brushes.Red;
                        item.Title = "    " + item.Title.Trim();
                        item.IsExpanded = true;
                    }
                    else
                    {
                        item.CheckColor = System.Windows.Media.Brushes.Black;
                        item.Title = "    " + item.Title.Trim();
                        item.IsExpanded = false;

                    }
                }
            }
        }

        public ObservableCollection<MenuConfig> menuConfigs { get; set; }
        public ObservableCollection<MenuConfig> MenuConfigs
        {
            get { return menuConfigs; }
            set
            {
                menuConfigs = value;
                OnPropertyChanged("MenuConfigs");
            }
        }

        private string userName { get; set; }
        public string UserName
        {
            get { return "欢迎您，" + userName + "！"; }
            set
            {
                userName = value;
                OnPropertyChanged("UserName");
            }
        }

        private string realName { get; set; }
        public string RealName
        {
            get { return "欢迎您，" + realName + "！"; }
            set
            {
                realName = value;
                OnPropertyChanged("RealName");
            }
        }
        private string tabName { get; set; }
        public string TabName
        {
            get { return tabName; }
            set
            {
                tabName = value;
                OnPropertyChanged("TabName");
            }
        }

        /// <summary>
        /// 当前页面
        /// </summary>
        private UserControl currentPage = new DefaultPage();
        public UserControl CurrentPage
        {
            get { return currentPage; }
            set
            {
                currentPage = value;
                OnPropertyChanged("CurrentPage");
            }
        }
    }
}
