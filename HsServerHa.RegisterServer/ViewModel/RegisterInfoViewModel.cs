﻿using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.Messaging.Messages;
using CommunityToolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HsServerHa.Entity.Models;
using HsServerHa.Utility.DataHelper;
using HsServerHa.Utility.SQLite;
using System.Collections.ObjectModel;
using System.Data;
using AduSkin.Controls.Metro;
using HsServerHa.Core.Http;
using HsServerHa.Utility.HttpHelper;
using Newtonsoft.Json;
using System.Configuration;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using HsServerHa.Utility.Computer;

namespace HsServerHa.RegisterServer.ViewModel
{
    public class RegisterInfoViewModel : ObservableObject
    {
        public RegisterInfoViewModel() {

        
            InitData();
            AddCommand = new RelayCommand(addFun);
            QueryCommand = new RelayCommand(InitData);
            DelCommand = new RelayCommand<object>(DelFun);
            UpdateCommand = new RelayCommand<object>(UpdateFun);

            WeakReferenceMessenger.Default.Register<ValueChangedMessage<string>, string>(this, "RefreshProgram", (r, message) =>
            {
                if (message != null)
                {
                    if (message.Value == "刷新授权信息数据")
                    {
                        InitData();
                    }
                }
            });
        }
        public void InitData()
        {
            var dataSet = DbHelperSQLite.Query("SELECT * FROM AuthManagement ", null);
            DataTable dt = dataSet.Tables[0];
            List<AuthManagementEntity> list = DataHelpers.ConvertToModel<AuthManagementEntity>(dt);

            AuthList = new ObservableCollection<AuthManagementEntity>(list);
        }

        public ObservableCollection<AuthManagementEntity> authList { get; set; }
        public ObservableCollection<AuthManagementEntity> AuthList
        {
            get { return authList; }
            set
            {
                authList = value;
                OnPropertyChanged("AuthList");
            }
        }

        public void addFun()
        {
            var dt = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
            var key = MD5EncryptHelper.MD5Encrypt16(dt);
            DbHelperSQLite.ExecuteSql($"insert into AuthManagement (Licensekey,IsUse,CreateTime) values('{key}','否','{dt}')");
            AduMessageBoxs.Show("新增成功！");
            InitData();

            //AlertTemp alertTempJoinMaster = new AlertTemp(new AddProgram(), "添加程序", "500", "800");
            //alertTempJoinMaster.ShowDialog();
        }
        public void UpdateFun(object obj)
        {
            //AlertTemp alertTempJoinMaster = new AlertTemp(new UpdateProgram(long.Parse(obj.ToString())), "修改程序", "500", "800");
            //alertTempJoinMaster.ShowDialog();
        }

        public async void DelFun(object obj)
        {
            MessageBoxResult dr = AduMessageBox.ShowOKCancel($"你确定删除吗？", "系统提示!", "确定", "取消");
            if (dr == MessageBoxResult.OK)
            {
                var id = obj.ToString();
                var firstPro = AuthList.FirstOrDefault(x => x.Id == long.Parse(id));

                if (firstPro == null)
                {
                    AduMessageBoxs.Show("数据不存在，请检查");
                    LoggerHelper.SetInfo("数据不存在->查询ID为：" + id, "启动程序管理");
                    return;
                }
                var dtBind = DbHelperSQLite.Query("select  * from MasterSlaveBind")?.Tables?[0];
        
                var result = new HttpResponseResultModel<object>();
                if (result == null)
                {
                    AduMessageBoxs.Show("删除失败！");
                    LoggerHelper.SetInfo("删除失败:" + JsonConvert.SerializeObject(firstPro), "启动程序管理");
                    return;
                }
                if (!result.IsSuccess)
                {
                    AduMessageBoxs.Show("删除失败！");
                    LoggerHelper.SetInfo("删除失败:" + JsonConvert.SerializeObject(firstPro), "启动程序管理");
                    return;
                }

                DbHelperSQLite.ExecuteSql($"delete from ProgramManage where Id={id}");

                InitData();
                AduMessageBoxs.Show("删除成功！");
                LoggerHelper.SetInfo("删除了程序数据:" + JsonConvert.SerializeObject(firstPro), "启动程序管理");
                return;

            }
        }
        public RelayCommand AddCommand { get; set; }
        public RelayCommand QueryCommand { get; set; }
        public RelayCommand<object> DelCommand { get; set; }
        public RelayCommand<object> UpdateCommand { get; set; }
    }
}
