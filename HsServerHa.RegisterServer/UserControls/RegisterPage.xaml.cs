﻿using AduSkin.Controls.Metro;
using HsServerHa.Entity.Models;
using HsServerHa.Utility.Computer;
using HsServerHa.Utility.DataHelper;
using HsServerHa.Utility.SQLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HsServerHa.RegisterServer.UserControls
{
    /// <summary>
    /// RegisterPage.xaml 的交互逻辑
    /// </summary>
    public partial class RegisterPage : UserControl
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private void btn_Setting_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var key = this.RequestCode.Text;
                if (string.IsNullOrEmpty(key))
                {
                    AduMessageBoxs.Show("请求码不能为空！");
                    return;
                }
                //解密
                var jmKey = RegistryHelper.DecryptString(key, RegistryHelper.key);
                //截取长度16位
                var licensekey = jmKey.Substring(0, 16);
                //查询数据库是否存在
                var dataSet = DbHelperSQLite.Query($"SELECT * FROM AuthManagement where Licensekey='{licensekey}' and IsUse='否'", null);
                DataTable dt = dataSet.Tables[0];
                List<AuthManagementEntity> list = DataHelpers.ConvertToModel<AuthManagementEntity>(dt);
                if (list.Count() <= 0)
                {
                    AduMessageBoxs.Show("生成失败,请检查请求码格式是否正确或已被使用！");
                    return;
                }
                var pcInfoKey = jmKey.Replace(licensekey, "");

                var ActivationCode = RegistryHelper.EncryptString(pcInfoKey, RegistryHelper.RegisterKey);
                if (string.IsNullOrEmpty(ActivationCode))
                {
                    AduMessageBoxs.Show("生成失败！");
                    return;
                }
                var sql = $"update AuthManagement set RequestCode='{key}',ActivationCode='{ActivationCode}',IsUse='是' where  Licensekey='{licensekey}'";
                var successResult = DbHelperSQLite.ExecuteSql(sql, "");
                if (successResult != 0)
                {
                    AduMessageBoxs.Show("生成成功！");
                    this.ActivationCode.Text = ActivationCode;
                    return;
                }
                else
                {
                    this.ActivationCode.Text = "";
                    AduMessageBoxs.Show("生成失败！");
                    return;
                }
            }
            catch (Exception ex)
            {
                AduMessageBoxs.Show(ex.Message, "异常", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }


        }
    }
}
