﻿using AduSkin.Controls.Metro;
using HsServerHa.Entity.Models;
using HsServerHa.RegisterServer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HsServerHa.RegisterServer.UserControls
{
    /// <summary>
    /// RegisterInfo.xaml 的交互逻辑
    /// </summary>
    public partial class RegisterInfo : UserControl
    {
        public RegisterInfo()
        {
            InitializeComponent();
            this.DataContext = new RegisterInfoViewModel();
            this.MyDataGrid.LoadingRow += new EventHandler<DataGridRowEventArgs>(this.dataGridEquipment_LoadingRow);
        }
        private void dataGridEquipment_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex() + 1;
        }

        private void MyDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var grid = e.Source as AduDataGrid;
            DataGridColumn? column = grid?.CurrentColumn;
            var item = grid?.CurrentItem as AuthManagementEntity;
            if (item == null) return;
            TextBlock? cellValue = column?.GetCellContent(item) as TextBlock ;
            var needText = cellValue?.Text;
            Clipboard.SetDataObject(needText);
            AduMessageBoxs.Show($"复制{column?.Header}成功！","提示");
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var grid = e.Source as AduDataGrid;
            var item = grid.CurrentItem as AuthManagementEntity;
        }
    }
}
