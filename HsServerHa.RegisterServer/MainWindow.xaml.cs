﻿using AduSkin.Controls.Metro;
using CommunityToolkit.Mvvm.Messaging;
using HsServerHa.RegisterServer.ViewModel;
using System;
using System.Windows.Media;

namespace HsServerHa.RegisterServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainWindowViewModel();
        }

        private void ChangeMenu_Click(object sender, EventArgs e)
        {
            var tag = (sender as MetroExpander).Tag.ToString();
            if (!string.IsNullOrEmpty(tag))
            {
                this.tabName.Content = (sender as MetroExpander).Header.ToString().Trim();
                if (tag == "DefaultPage")
                {

                    this.Default.Foreground = Brushes.Red;
                    this.Default.Background = Brushes.Red;
                }
                else
                {
                    this.Default.Foreground = Brushes.Black;
                }

            }

            WeakReferenceMessenger.Default.Send(tag, "MenuData");
        }
    }
}
