﻿using System.IO;

namespace HsServerHa.RegisterServer.Common
{
    public static class CustomConfigManager
    {
        //读取配置文件：appsettings
        //1.Microsoft.Extensions.Configuration；
        //2.Microsoft.Extensions.Configuration.Json； 
        public static string GetMenuConfig()
        {
            var a = File.ReadAllText("Config\\MenuConfig.json");
            return a;
        }

    }
}
