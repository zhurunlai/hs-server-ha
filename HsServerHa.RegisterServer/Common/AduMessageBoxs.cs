﻿using AduSkin.Controls.Metro;
using HsServerHa.RegisterServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HsServerHa
{
    public class AduMessageBoxs
    {
        public static void Show(string msg, string caption = "", MessageBoxButton button = MessageBoxButton.OK, MessageBoxImage messageBoxImage = MessageBoxImage.None)
        {

            App.Current.Dispatcher.Invoke((Action)(() =>
            {
                AduMessageBox.Show(msg, caption, button, messageBoxImage);
            }));
        }
    }
}
