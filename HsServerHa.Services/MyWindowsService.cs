﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Timers;
//using HsServerHa.Utility;

namespace HsServerHa.Services
{
    public partial class MyWindowsService : ServiceBase
    { 
        public MyWindowsService()
        {
            InitializeComponent();
        }

        private const string FilePath = "D:\\net472\\MaterialDesignDemo.exe"; // 指定要监控的程序名称
        private const string FilePathSrc = "D:\\net472"; // 指定要监控的程序名称
        private const string FileName = "MaterialDesignDemo"; // 指定要监控的程序名称

        private Process wpfProcess;
        protected override void OnStart(string[] args)
        {
            // 启动WPF程序
            StartWPFProgram();
        }
        protected override void OnStop()
        {
            // 停止WPF程序
            StopWPFProgram();
        }

        private void StartWPFProgram()
        {
            // 启动WPF程序的进程
            wpfProcess = new Process();
            wpfProcess.StartInfo.FileName =FilePath; // 替换为您的WPF程序的路径
            wpfProcess.Start();

            // 监控WPF程序的状态
            ThreadPool.QueueUserWorkItem(state =>
            {
                wpfProcess.WaitForExit();
                // 当WPF程序关闭时，重新启动它
                StartWPFProgram();
            });
        }

        private void StopWPFProgram()
        {
            // 停止WPF程序的进程
            if (wpfProcess != null && !wpfProcess.HasExited)
            {
                wpfProcess.Kill();
            }
        }

        //private Process wpfProcess;
        //protected override void OnStart(string[] args)
        //{
        //    // 启动WPF程序
        //    wpfProcess = new Process();
        //    wpfProcess.StartInfo.FileName = FilePath;
        //    wpfProcess.EnableRaisingEvents = true;
        //    wpfProcess.Exited += WpfProcess_Exited;
        //    wpfProcess.Start();
        //}

        //private void WpfProcess_Exited(object sender, EventArgs e)
        //{
        //    // WPF程序被关闭时重新启动
        //    wpfProcess.Start();
        //}

        //protected override void OnStop()
        //{
        //    //// 停止WPF程序
        //    //if (wpfProcess != null && !wpfProcess.HasExited)
        //    //{
        //    //    wpfProcess.Kill();
        //    //}
        //}


        //private readonly Timer _timer = new Timer();
        //protected override void OnStart(string[] args)
        //{ 
        //    LoggerHelper.Info($"{DateTime.Now},服务启动");
        //    _timer.Interval = 5000; // 设置定时器间隔为5秒
        //    _timer.Elapsed += Timer_Elapsed; // 绑定定时器事件
        //    _timer.Start(); // 启动定时器
        //}
        //public  void Timer_Elapsed(object sender, ElapsedEventArgs e) {
        //    _timer.Stop(); // 停止定时器，避免重复触发
        //    LoggerHelper.Info($"服务执行中~"); 
        //    if (!IsProcessRunning(FileName))
        //    {
        //        StartProcess(FilePath, FilePathSrc); // 如果指定程序未运行，则启动该程序
        //    }
        //    _timer.Start(); // 重新启动定时器
        //}

        //private void StartProcess(string processPath,string FilePathSrc)
        //{
        //    try
        //    {
        //        //string wpfAppPath = "YourWPFApp.exe"; // 替换为您的 WPF 程序路径

        //        ProcessStartInfo startInfo = new ProcessStartInfo(processPath);
        //        startInfo.UseShellExecute = false; // 必须为 false
        //        startInfo.CreateNoWindow = false; // 必须为 false
        //        startInfo.WorkingDirectory = FilePathSrc;
        //        Process.Start(startInfo);
        //    }
        //    catch (Exception ex)
        //    {
        //        // 处理启动程序失败的异常
        //    }
        //}
        ///// <summary>
        ///// 检查程序是否运行
        ///// </summary>
        ///// <param name="path"></param>
        ///// <returns></returns>
        //private bool IsProcessRunning(string FileName)
        //{
        //    LoggerHelper.Info($"{FileName}");
        //    Process[] processes = Process.GetProcesses();
        //    foreach (Process process in processes)
        //    {
        //        if (process.ProcessName == FileName)
        //        {
        //            LoggerHelper.Info($"存在！");
        //            return true;
        //        }
        //    }
        //    LoggerHelper.Info($"不存在！");
        //    return false;
        //}

        //protected override void OnStop()
        //{
        //    _timer.Stop(); // 停止定时器
        //    LoggerHelper.Info($"{DateTime.Now},服务停止");
        //}
    }
}
