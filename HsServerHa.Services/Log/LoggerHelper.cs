﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace HsServerHa
{
    public static class LoggerHelper
    {



        /// <summary>
        /// 写日志
        /// </summary>
        /// <param name="logStr"></param>
        public static async Task WriteLogsAsync(string logStr, string fileDirectory = "")
        {

            try
            {
                //string path1 = Environment.CurrentDirectory + Path.Combine("/logs/") + fileDirectory;
                string path1 = AppDomain.CurrentDomain.BaseDirectory + Path.Combine("/logs/") + fileDirectory;
                if (!Directory.Exists(path1))
                {
                    //创建索引目录
                    Directory.CreateDirectory(path1);
                }
                string path = path1 + "/" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                FileStream stream = null;
                if (!File.Exists(path))
                {
                    stream = new FileStream(path, FileMode.Create);
                }
                else
                {
                    stream = new FileStream(path, FileMode.Append);
                }
                StreamWriter writer = new StreamWriter(stream);
                await writer.WriteAsync(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + ":  " + logStr + Environment.NewLine);
                writer.Write(Environment.NewLine);
                writer.Flush();
                writer.Close();
                stream.Close();
            }
            catch
            {
            }
            finally
            {

            }
        }


        public static async void Info(string msg,string fileName="")
        {
            if (string.IsNullOrEmpty(fileName))
            {
                await WriteLogsAsync(msg, "Info").ConfigureAwait(false);
            }
          
            else
            {
                await WriteLogsAsync(msg, fileName+"-Info").ConfigureAwait(false);
            }
        }
        public static async void Error(string msg, string fileName="")
        {
            if (string.IsNullOrEmpty(fileName))
            {
                await WriteLogsAsync(msg, "Error").ConfigureAwait(false);
            }

            else
            {
                await WriteLogsAsync(msg, fileName + "-Error").ConfigureAwait(false);
            }
        }

        public static async void Warn(string msg, string fileName="")
        {
            if (string.IsNullOrEmpty(fileName))
            {
                await WriteLogsAsync(msg, "Warn").ConfigureAwait(false);
            }

            else
            {
                await WriteLogsAsync(msg, fileName + "-Warn").ConfigureAwait(false);
            }
        }
    }
}
